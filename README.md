# Chronicles.

A notepad app designed to keep one's life well diary-documented
as well as pager-organised.

# Available features:

* Notes containing title, body and datespan;
* categories to keep notes organised;
* archive and trashbin as an another way of keeping them organised;
* note browser capable of filtering through notes based on their properties,
as well as group-modifying them;
* note editor made for dynamic note edition;
* note assistant always idling up there in the notification bar for you to
quickly influence or access the editor;
* (WIP) checklists which one can start up in every note. 

# Changelog:

* 21-07-2018 - v.0.9.2:

    * Settings partly reworked - now you can set default noteColumnOrder which will
    initially remain every time you run the app; the same with browserVisibleCategories;
    * bugfixes? UI tweaks?

* 14-07-2018 - v.0.8.9:

    * Checklists can now be user-ordered - by drag-and-dropping individual items
    one changes their order; non-checked items also appear earlier on the list
    than checked items;
    * small UI changes.

* 11-07-2018 - v.0.8.8.1:

    * Small bugfix.

* 11-07-2018 - v.0.8.8:

    * Tidied up the framework regarding notes - now it's a simple inheritance
    Bundleable (abstract interface) < LooseNote (runtime) < (Selectable) FirmNote
    (database); modified the browser and the editor so that they fit this framework;
    * with rebuilding of the note framework came an improvement in the editor and its tracking
    of whether the note (and its checklist) is modified or not - the editor now properly
    checks, whether anything has been added/modified/deleted in the checklist, and is able
    to properly revert;
    * built a framework of highly abstract filters and order, which provide Predicates
    used to filter notes (and other things in the future) as well as SQLite clauses;
    the browser uses them to store currently applied filters and order and, by using them,
    properly ask the database as well as dynamically check its notes, if they are correctly
    ordered and filtered;
    * with constructing the filter framework came an improvement to the browser and its
    reactions to any changes in any notes; it's now able to properly react to a note
    newly added/modified/deleted from the database - track whether something has to change
    in its list, where it has to change and how; it was unfinished and buggy before;
    * simple one-column order has been replaced by multi-column order - every note column
    has its influence - one just states, in which order they are checked;
    * added a graphic effect to swiping notes horizontally in the browser.

* 26-06-2018 - v.0.8.7:

    * Alpha prototype of checklists; every note has a possibility to have a checklist,
    there's a list in the Editor, right below the content textfield; checklist's items
    are for now provisionally ordered in creation order (by ID); for now you HAVE TO
    save the note for the checklist to be updated into the database (the checklist doesn't
    track its note's 'modified state' yet).
    
* 19-06-2018 - v.0.8.3:

    * The same bugfix third time..
    * NoteAssistant now should not force Editor creation; instead, he should monitor,
    if Editor is present; if it is, the Assistant will show current note; else, it
    will show information used in previous versions: 'no note is loaded'.
    
* 18-06-2018 - v.0.8.2:

    * Bugfix - seems like the previous bugfix wasn't debugged.
    
* 17-06-2018 - v.0.8.1:

    * Bugfix - situation, when we're in the Editor, we initialize new note from the Assistant,
    the Editor properly updates, but when we build our new note and try to save it, app
    crashes (although the new note is properly added into the database);
    * added new icons with the Second Generation Deer.
    
* 17-06-2018 - v.0.8.0:

    * _Major_ app structure rebuild - bugfixes and performance improvements;
    * improved GUI aesthetics - improved fonts, deer icon, notes as cards, etc;
    * improved GUI functionality - swipable notes as cards, revertable swipes,
    start- and end- dates quickly available to edit in editor;
    * actually started versioning.
    
# Bugs:
    
* Regex search with some complex patterns may crash (or even worse: freeze);
* still problems with finding right position in the browser for a freshly updated note;
* specifically modified checklist upon revertion may arrange in a different order
than it should be (and would be, if loaded from database again);

* FIXME: when one creates a new category and right after that
creates some notes in that category/moves existent notes to it, they won't appear/disappear,
 because browser filter hasn't been updated yet, as well as default browserCategoryVisibility.

# Minor TODOs

* Clearable dateStart and dateEnd;
* group rename, regex replace, group recategory etc.

# Major TODOs:

1. backend:
    1. reminders system;
    1. associations system;
    1. cloud sync, backup system;
    1. import/export notes function;
    1. media system;
1. frontend:
    1. more convenient datetime pickers;
    1. calendar tab;
    1. timeline tab;
    1. association map tab;
