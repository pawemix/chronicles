package pawemix.chronicles.controller;

import android.util.ArraySet;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import pawemix.chronicles.controller.editor.SimpleCLEditorController;
import pawemix.chronicles.model.NoteProvider;
import pawemix.chronicles.model.ObservableChecklistProvider;
import pawemix.chronicles.model.ObservableNoteProvider;

public class ControllerStack {

    private static final List<SimpleCLEditorController> clEditors = new LinkedList<>();
    private static final List<Set<Object>> clEditorUsers = new LinkedList<>();

    private static final List<ArrayListBrowserController> browsers = new LinkedList<>();
    private static final List<Set<Object>> browserUsers = new LinkedList<>();

    private static final List<ControllerStackObserver> observers = new LinkedList<>();

    // ======== CLEDITOR ======== //

    public static SimpleCLEditorController fetchCLEditor(int position, Object user, ObservableNoteProvider noteProvider, ObservableChecklistProvider checklistProvider) {

        if(position < 0) throw new ArrayIndexOutOfBoundsException(position);

        while(Math.min(clEditors.size(), clEditorUsers.size()) <= position) {

            clEditors.add(null);
            clEditorUsers.add(null);
        }

        if(clEditors.get(position) == null) {

            clEditors.set(position, new SimpleCLEditorController(noteProvider, checklistProvider));
            clEditorUsers.set(position, new ArraySet<>(5));

            notifyCLEditorCreated(position, clEditors.get(position));
        }

        clEditorUsers.get(position).add(user);
        return clEditors.get(position);
    }

    public static SimpleCLEditorController peekCLEditor(int position) {

        if(position < 0) throw new ArrayIndexOutOfBoundsException(position);
        if(position < clEditors.size()) return clEditors.get(position);
        return null;
    }

    public static void abandonCLEditor(int position, Object user) {

        if(position > -1 && position < clEditorUsers.size() && clEditorUsers.get(position) != null) {

            clEditorUsers.get(position).remove(user);

            if(clEditorUsers.get(position).isEmpty()) {

                clEditors.set(position, null);
                clEditorUsers.set(position, null);

                notifyCLEditorDied(position);
            }
        }
    }

    // ======== BROWSER ======== //

    public static ArrayListBrowserController fetchBrowser(int position, Object user, NoteProvider provider) {

        if(position < 0) throw new ArrayIndexOutOfBoundsException(position);

        while(Math.min(browsers.size(), browserUsers.size()) <= position) {

            browsers.add(null);
            browserUsers.add(null);
        }

        if(browsers.get(position) == null) {

            browsers.set(position, new ArrayListBrowserController(provider));
            browserUsers.set(position, new ArraySet<>(5));

            notifyBrowserCreated(position, browsers.get(position));
        }

        browserUsers.get(position).add(user);
        return browsers.get(position);
    }

    public static void abandonBrowser(int position, Object user) {

        if(position > 0 && position < browserUsers.size() && browserUsers.get(position) != null) {

            browserUsers.get(position).remove(user);

            if(browserUsers.get(position).isEmpty()) {

                browsers.set(position, null);
                browserUsers.set(position, null);

                notifyBrowserDied(position);
            }
        }
    }

    // ======== Stack Observable ======== //

    public static void addObserver(ControllerStackObserver observer) {

        observers.add(observer);
    }

    public static void removeObserver(ControllerStackObserver observer) {

        observers.remove(observer);
    }

    private static void notifyCLEditorCreated(int position, SimpleCLEditorController controller) {

        for(ControllerStackObserver observer : observers) observer.onCLEditorCreated(position, controller);
    }

    private static void notifyCLEditorDied(int position) {

        for(ControllerStackObserver observer : observers) observer.onCLEditorDied(position);
    }

    private static void notifyBrowserCreated(int position, ArrayListBrowserController controller) {

        for(ControllerStackObserver observer : observers) observer.onBrowserCreated(position, controller);
    }

    private static void notifyBrowserDied(int position) {

        for(ControllerStackObserver observer : observers) observer.onBrowserDied(position);
    }
}
