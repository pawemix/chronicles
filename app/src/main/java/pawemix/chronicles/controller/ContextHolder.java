package pawemix.chronicles.controller;

import android.content.Context;

public interface ContextHolder {
    Context getAppContext();
}
