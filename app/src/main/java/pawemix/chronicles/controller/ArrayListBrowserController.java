package pawemix.chronicles.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.filter.ColumnOrder;
import pawemix.chronicles.framework.filter.EqualsFilter;
import pawemix.chronicles.framework.filter.Filter;
import pawemix.chronicles.framework.filter.InFilter;
import pawemix.chronicles.framework.filter.NullFilter;
import pawemix.chronicles.framework.filter.RegexFilter;
import pawemix.chronicles.framework.note.FirmNote;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.NoteObserver;
import pawemix.chronicles.model.NoteProvider;
import pawemix.chronicles.model.NoteProviderObservable;

public class ArrayListBrowserController extends RawObservableBrowserController implements NoteObserver, BrowserController {

    private NoteProvider provider;
    private List<FirmNote> notes = new ArrayList<>();

    private EqualsFilter<Bundleable> sectionFilter;
    private InFilter<Bundleable> categoryFilter;
    private RegexFilter<Bundleable> textFilter;

    private ColumnOrder<Bundleable> order;
    private Filter<Bundleable> filter;
    private boolean autoReload = true;

    ArrayListBrowserController(NoteProvider provider) {
        super();
        if(provider instanceof NoteProviderObservable)
            ((NoteProviderObservable) provider).addObserver(this);
        this.provider = provider;

        List<String> categories = new ArrayList<>(1);
        categories.add(DB.DEFAULTS_CATEGORY);

        sectionFilter = new EqualsFilter<>(new NullFilter<>(), DB.SECTION_DEFAULT, DB.NOTES_SECTION);
        categoryFilter = new InFilter<>(sectionFilter, categories, DB.NOTES_CATEGORY);
        textFilter = new RegexFilter<>(categoryFilter, Pattern.compile(".*"), DB.NOTES_TITLE, DB.NOTES_CONTENT);

        order = new ColumnOrder<>(provider.getNoteColumns(), true);
        filter = textFilter;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(this.provider instanceof NoteProviderObservable)
            ((NoteProviderObservable) this.provider).removeObserver(this);
        this.provider = null;
    }

    @Override
    public int positionOf(long noteID) {
        for(FirmNote note : notes)
            if(note.getID() == noteID)
                return notes.indexOf(note);
        return -1;
    }

    @Override
    public void deleteSelected() {
        Collection<Long> idsToDelete = new ArrayList<>();

        for(FirmNote note : notes) if(note.isSelected()) idsToDelete.add(note.getID());
        provider.deleteNotes(idsToDelete);
    }

    @Override
    public void selectAll() {
        for(FirmNote note : notes) note.setSelected(true);
        notifyNotesReloaded();
    }

    @Override
    public void deselectAll() {
        for(FirmNote note : notes) note.setSelected(false);
        notifyNotesReloaded();
    }

    @Override
    public void swapSelectionForNoteAt(int position) {
        FirmNote note = notes.get(position);
        note.setSelected(!note.isSelected());
        notifyNoteUpdatedAt(position);
    }

    @Override
    public boolean allNotesDeselected() {
        boolean result = true;
        for(FirmNote note : notes) {
            if(note.isSelected()) {
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public void selectNoteAt(int position) {
        if(position >= 0 && position < notes.size()) {
            notes.get(position).setSelected(true);
            notifyNoteUpdatedAt(position);
        }
    }

    @Override
    public void reloadNotes() {

        notes = provider.fetchNotes(filter.clause(), filter.args(), order.clause());

        notifyNotesReloaded();
    }

    @Override
    public int getNotesAmount() {
        return notes.size();
    }

    @Override
    public FirmNote getNoteAt(int position) {
        return notes.get(position);
    }

    @Override
    public void restoreSelected() {

        Collection<FirmNote> toRestore = new ArrayList<>();
        for(FirmNote note : notes) if(note.isSelected()) toRestore.add(note);
        for(FirmNote note : toRestore) provider.restoreNote(note.getID());
    }

    @Override
    public void archiveSelected() {

        Collection<FirmNote> toArchive = new ArrayList<>();
        for(FirmNote note : notes) if(note.isSelected()) toArchive.add(note);
        for(FirmNote note : toArchive) provider.archiveNote(note.getID());
    }

    @Override
    public void trashSelected() {

        Collection<FirmNote> toTrash = new ArrayList<>();
        for(FirmNote note : notes) if(note.isSelected()) toTrash.add(note);
        for(FirmNote note : toTrash) provider.trashNote(note.getID());
    }

    @Override
    public void archive(long id) {
        provider.archiveNote(id);
    }

    @Override
    public void trash(long id) {
        provider.trashNote(id);
    }

    @Override
    public void restore(long id) {
        provider.restoreNote(id);
    }

    // ======== FILTERS AND ORDER ======== //

    @Override
    public void setAutoReload(boolean b) {
        this.autoReload = b;
    }

    @Override
    public List<String> getSelectedCategoryNames() {
        return categoryFilter.getValues();
    }

    @Override
    public void setSelectedCategoryNames(List<String> names) {
        categoryFilter.setValues(names);
        if(autoReload) reloadNotes();
    }

    @Override
    public void setSection(String section) {
        this.sectionFilter.setValue(section);
        if(autoReload) reloadNotes();
    }

    @Override
    public void setTextFilter(Pattern pattern) {
        this.textFilter.setPattern(pattern);
        if(autoReload) reloadNotes();
    }

    @Override
    public Pattern getTextFilter() {
        return this.textFilter.getPattern();
    }

    @Override
    public void setOrder(ColumnOrder<Bundleable> order) {

        this.order = order;

        Collections.sort(notes, order.comparator());

        notifyNotesReloaded();
    }

    @Override
    public List<String> getColumnOrder() {
        return order.getColumns();
    }

    @Override
    public List<Boolean> getColumnAscendings() {
        return order.getAscendings();
    }

    // ======== ############################## ======== //
    // ======== Metody obserwujące bazę danych ======== //
    // ======== ############################## ======== //

    @Override
    public void onNoteUpdated(FirmNote old, FirmNote updated) {

        int position = Collections.binarySearch(notes, old, order.comparator());

        // 1st case: jest na liście
        if(position > -1) {

            // po aktualizacji będzie pasował do filtrów
            if(filter.predicate().test(updated)) {

                int newPosition = Collections.binarySearch(notes, updated, order.comparator());

                if(newPosition < 0) newPosition = -newPosition - 1;

                // po aktualizacji zostanie na tym samym miejscu
                if(newPosition == position) {
                    notes.get(position).updateBundle(updated.toBundle());
                    notifyNoteUpdatedAt(position);
                }

                // po aktualizacji się przesunie
                else {
                    FirmNote moved = notes.get(position);
                    moved.updateBundle(updated.toBundle());
                    notes.remove(position);
                    notes.add(newPosition, moved);
                    notifyNoteMovedBetween(position, newPosition);
                }
            }

            // po aktualizacji nie będzie pasował do filtrów
            else {
                notes.remove(position);
                notifyNoteRemovedAt(position);
            }
        }

        // 2nd base: nie ma na liście, ale pasuje do filtrów
        else if(filter.predicate().test(updated)) {

            position = -Collections.binarySearch(notes, updated, order.comparator()) - 1;
            FirmNote inserted = new FirmNote(old.getID(), updated.toBundle());
            notes.add(position, inserted);
            notifyNoteInsertedAt(position);
        }
    }

    @Override
    public void onNotesUpdated(List<FirmNote> olds, List<FirmNote> updateds) {

        int size = Math.min(olds.size(), updateds.size());
        for(int i = 0; i < size; i++) onNoteUpdated(olds.get(i), updateds.get(i));
    }

    @Override
    public void onNoteCreated(FirmNote created) {

        if(textFilter.predicate().test(created)) {
            int position = -Collections.binarySearch(notes, created, order.comparator()) - 1;
            FirmNote note = new FirmNote(created.getID(), created.toBundle());
            notes.add(position, note);
            notifyNoteInsertedAt(position);
        }
    }

    @Override
    public void onNotesCreated(List<FirmNote> createds) {
        for(int i = 0; i < Math.min(createds.size(), createds.size()); i++) onNoteCreated(createds.get(i));
    }

    @Override
    public void onNoteDeleted(FirmNote deleted) {

        if(textFilter.predicate().test(deleted)) {
            int position = Collections.binarySearch(notes, deleted, order.comparator());
            notes.remove(position);
            notifyNoteRemovedAt(position);
        }
    }

    @Override
    public void onNotesDeleted(Collection<FirmNote> deleteds) {
        for(FirmNote deleted : deleteds) onNoteDeleted(deleted);
    }
}
