package pawemix.chronicles.controller;

public interface ObservableCategoriesController extends CategoriesController {

    void addObserver(CategoriesObserver observer);

    void removeObserver(CategoriesObserver observer);
}
