package pawemix.chronicles.controller;

import java.util.HashSet;
import java.util.Set;

import pawemix.chronicles.framework.category.Category;

abstract class RawObservableCategoriesController implements ObservableCategoriesController {

    private Set<CategoriesObserver> observers = new HashSet<>();

    @Override
    public void addObserver(CategoriesObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(CategoriesObserver observer) {
        observers.remove(observer);
    }

    protected void notifyCategoriesReloaded() {
        for(CategoriesObserver observer : observers) observer.onCategoriesReloaded();
    }

    protected void notifyCategoryUpdated(int position, Category category) {
        for(CategoriesObserver observer : observers) observer.onCategoryUpdated(position, category);
    }
}
