package pawemix.chronicles.controller;

import java.util.List;
import java.util.regex.Pattern;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.filter.ColumnOrder;
import pawemix.chronicles.framework.note.FirmNote;

public interface BrowserController {
    int positionOf(long noteId);

    void deleteSelected();

    void selectAll();

    void deselectAll();

    void swapSelectionForNoteAt(int position);

    boolean allNotesDeselected();

    void selectNoteAt(int position);

    void reloadNotes();

    int getNotesAmount();

    FirmNote getNoteAt(int position);

    void restoreSelected();

    void archiveSelected();

    void trashSelected();

    void archive(long id);

    void trash(long id);

    void restore(long id);

    List<String> getSelectedCategoryNames();

    void setSelectedCategoryNames(List<String> names);

    void setOrder(ColumnOrder<Bundleable> order);

    void setAutoReload(boolean b);

    void setSection(String section);

    void setTextFilter(Pattern pattern);

    Pattern getTextFilter();

    List<String> getColumnOrder();

    List<Boolean> getColumnAscendings();
}
