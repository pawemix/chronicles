package pawemix.chronicles.controller;

import java.util.HashSet;
import java.util.Set;

public abstract class RawObservableBrowserController implements ObservableBrowserController {

    private Set<BrowserObserver> observers = new HashSet<>();

    @Override
    public void addObserver(BrowserObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(BrowserObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyNoteUpdatedAt(int position) {
        for(BrowserObserver observer : observers) observer.onNoteUpdatedAt(position);
    }

    @Override
    public void notifyNoteRemovedAt(int position) {
        for(BrowserObserver observer : observers) observer.onNoteRemovedAt(position);
    }

    @Override
    public void notifyNotesReloaded() {
        for(BrowserObserver observer : observers) observer.onNotesReloaded();
    }

    @Override
    public void notifyNoteInsertedAt(int position) {
        for(BrowserObserver observer : observers) observer.onNoteInsertedAt(position);
    }

    @Override
    public void notifyNoteMovedBetween(int from, int to) {
        for(BrowserObserver observer : observers) observer.onNoteMovedBetween(from, to);
    }
}
