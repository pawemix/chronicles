package pawemix.chronicles.controller;

import pawemix.chronicles.controller.editor.SimpleCLEditorController;

public interface ControllerStackObserver {

    void onBrowserCreated(int position, ArrayListBrowserController controller);

    void onBrowserDied(int position);

    void onCLEditorCreated(int position, SimpleCLEditorController controller);

    void onCLEditorDied(int position);
}
