package pawemix.chronicles.controller;

import pawemix.chronicles.framework.category.Category;

public interface CategoriesObserver {

    void onCategoriesReloaded();

    void onCategoryUpdated(int position, Category category);
}
