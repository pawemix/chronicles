package pawemix.chronicles.controller;

import android.content.Context;

import pawemix.chronicles.R;

public class RawContextHolder implements ContextHolder {
    private Context appContext;

    public final String NOTES;
    public final String NOTES_ID;
    public final String NOTES_DATECREATED;
    public final String NOTES_DATEMODIFIED;
    public final String NOTES_TITLE;
    public final String NOTES_CONTENT;
    public final String NOTES_DATESTART;
    public final String NOTES_DATEEND;
    public final String NOTES_CATEGORY;
    public final String DEFAULTS_CATEGORY;
    public final String NOTES_SECTION;
    public final String DEFAULTS_SECTION;
    public final String NOTES_TYPE;
    public final String DEFAULTS_TYPE;
    public final String CATEGORIES;
    public final String CATEGORIES_NAME;
    public final String CATEGORIES_COLORKEY;
    public final String DEFAULTS_COLORKEY;
    public final String CATEGORIES_LIGHTCOLOR;
    public final String CATEGORIES_DARKCOLOR;
    public final String SECTIONS_DEFAULT;
    public final String SECTIONS_ARCHIVE;
    public final String SECTIONS_TRASHBIN;

    public RawContextHolder(Context appContext) {
        this.appContext = appContext;

        NOTES = appContext.getString(R.string.dbkeys_notes);
        NOTES_ID = appContext.getString(R.string.dbkeys_notes_id);
        NOTES_DATECREATED = appContext.getString(R.string.dbkeys_notes_datecreated);
        NOTES_DATEMODIFIED = appContext.getString(R.string.dbkeys_notes_datemodified);
        NOTES_TITLE = appContext.getString(R.string.dbkeys_notes_title);
        NOTES_CONTENT = appContext.getString(R.string.dbkeys_notes_content);
        NOTES_DATESTART = appContext.getString(R.string.dbkeys_notes_datestart);
        NOTES_DATEEND = appContext.getString(R.string.dbkeys_notes_dateend);
        NOTES_CATEGORY = appContext.getString(R.string.dbkeys_notes_category);
        DEFAULTS_CATEGORY = appContext.getString(R.string.defaults_notes_category);
        NOTES_SECTION = appContext.getString(R.string.dbkeys_notes_section);
        DEFAULTS_SECTION = appContext.getString(R.string.defaults_notes_section);
        NOTES_TYPE = appContext.getString(R.string.dbkeys_notes_type);
        DEFAULTS_TYPE = appContext.getString(R.string.defaults_type);
        CATEGORIES = appContext.getString(R.string.dbkeys_categories);
        CATEGORIES_NAME = appContext.getString(R.string.dbkeys_categories_name);
        CATEGORIES_COLORKEY = appContext.getString(R.string.dbkeys_categories_colorkey);
        CATEGORIES_DARKCOLOR = getAppContext().getString(R.string.dbkeys_categories_darkcolor);
        CATEGORIES_LIGHTCOLOR = getAppContext().getString(R.string.dbkeys_categories_lightcolor);
        DEFAULTS_COLORKEY = appContext.getString(R.string.defaults_colorkey);
        SECTIONS_DEFAULT = getAppContext().getString(R.string.sections_default);
        SECTIONS_ARCHIVE = getAppContext().getString(R.string.sections_archive);
        SECTIONS_TRASHBIN = getAppContext().getString(R.string.sections_trashbin);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        appContext = null;
    }

    @Override
    public Context getAppContext() {
        return appContext;
    }
}
