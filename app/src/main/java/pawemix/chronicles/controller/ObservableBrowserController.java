package pawemix.chronicles.controller;

public interface ObservableBrowserController extends BrowserController {
    void addObserver(BrowserObserver observer);

    void removeObserver(BrowserObserver observer);

    void notifyNoteUpdatedAt(int position);

    void notifyNoteRemovedAt(int position);

    void notifyNotesReloaded();

    void notifyNoteInsertedAt(int position);

    void notifyNoteMovedBetween(int from, int to);
}
