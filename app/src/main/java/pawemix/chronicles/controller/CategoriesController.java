package pawemix.chronicles.controller;

import java.util.List;

import pawemix.chronicles.framework.category.Category;

public interface CategoriesController {

    int getCategoriesAmount();

    Category getCategoryAt(int position);

    void deleteCategory(String deletedName, String surrogateName);

    boolean categoryNameIsFree(String name);

    void setCategoryName(String oldName, String newName);

    void setCategoryColorKey(String name, String key);

    void addCategory(String name, String key);

    List<String> getCategoryNames();

    boolean categoryHasNotes(String name);

    String getKeyOf(String name);
}
