package pawemix.chronicles.controller;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import pawemix.chronicles.framework.category.Category;
import pawemix.chronicles.framework.category.ChronicleCategory;
import pawemix.chronicles.model.CategoryObserver;
import pawemix.chronicles.model.CategoryProvider;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.ObservableCategoryProvider;

public class ArrayListCategoriesController extends RawObservableCategoriesController implements CategoryObserver, CategoriesController {

    private static ArrayListCategoriesController instance;

    private CategoryProvider provider;
    private List<Category> categories;

    private ArrayListCategoriesController(CategoryProvider provider) {
        this.provider = provider;
        if(provider instanceof ObservableCategoryProvider) ((ObservableCategoryProvider) provider).addObserver(this);
        this.categories = fetchCategories();
        notifyCategoriesReloaded();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(provider instanceof ObservableCategoryProvider) ((ObservableCategoryProvider) provider).removeObserver(this);
    }

    private List<Category> fetchCategories() {
        List<Bundle> rawCategories = provider.fetchCategories();
        List<Category> categories = new ArrayList<>(rawCategories.size());
        for(Bundle rawCategory : rawCategories)
            categories.add(new ChronicleCategory(rawCategory.getString(DB.CATEGORIES_NAME),
                    rawCategory.getString(DB.CATEGORIES_COLORKEY)
            ));
        return categories;
    }

    public static ArrayListCategoriesController getInstance(CategoryProvider provider) {
        if(instance == null) {
            synchronized(ArrayListCategoriesController.class) {
                if(instance == null) instance = new ArrayListCategoriesController(provider);
            }
        }
        return instance;
    }

    @Override
    public Category getCategoryAt(int position) {
        return categories.get(position);
    }

    @Override
    public boolean categoryHasNotes(String name) {
        return provider.categoryHasNotes(name);
    }

    @Override
    public String getKeyOf(String name) {
        for(Category category : categories)
            if(category.getName().equalsIgnoreCase(name))
                return category.getColorKey();
        return null;
    }

    @Override
    public void deleteCategory(String deletedName, String surrogateName) {
        provider.deleteCategory(deletedName, surrogateName);
    }

    @Override
    public boolean categoryNameIsFree(String name) {
        boolean found = false;
        for(Category category : categories) {
            if(category.getName().equalsIgnoreCase(name)) {
                found = true;
                break;
            }
        }
        return !found;
    }

    @Override
    public void setCategoryName(String oldName, String newName) {
        provider.setCategoryName(oldName, newName);
    }

    @Override
    public void setCategoryColorKey(String name, String key) {
        provider.setColorKey(name, key);
    }

    @Override
    public void addCategory(String name, String key) {
        provider.addCategory(name, key);
    }

    @Override
    public List<String> getCategoryNames() {
        List<String> output = new ArrayList<>(categories.size());
        for(Category category : categories) output.add(category.getName());
        return output;
    }

    @Override
    public int getCategoriesAmount() {
        return categories.size();
    }

    // ======== ########################## ======== //
    // ======== CATEGORY PROVIDER OBSERVER ======== //
    // ======== ########################## ======== //

    @Override
    public void onCategoryUpdated(String oldName, Bundle categoryBundle) {

        int position = 0;
        for(Category category : categories) {

            if(category.getName().equals(oldName)) {

                category.setName(categoryBundle.getString(DB.CATEGORIES_NAME));
                category.setColorKey(categoryBundle.getString(DB.CATEGORIES_COLORKEY));
                notifyCategoryUpdated(position, category);
                break;
            }

            position++;
        }
    }

    @Override
    public void onCategoryCreated(Bundle categoryBundle) {
        this.categories = fetchCategories();
        notifyCategoriesReloaded();
    }

    @Override
    public void onCategoryDeleted(Bundle categoryBundle) {
        this.categories = fetchCategories();
        notifyCategoriesReloaded();
    }
}