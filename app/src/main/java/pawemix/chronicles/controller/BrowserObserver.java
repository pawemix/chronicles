package pawemix.chronicles.controller;

public interface BrowserObserver {
    void onNoteUpdatedAt(int position);

    void onNoteRemovedAt(int position);

    void onNotesReloaded();

    void onNoteInsertedAt(int position);

    void onNoteMovedBetween(int from, int to);
}
