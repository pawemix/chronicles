package pawemix.chronicles.controller.editor;

public interface ObservableCLEditorController {

    void addObserver(CLEditorObserver observer);

    void removeObserver(CLEditorObserver observer);

    void notifyItemUpdated(int position);

    void notifyItemRemoved(int position);

    void notifyItemInserted(int position);

    void notifyItemMoved(int from, int to);

    void notifyItemsReloaded();
}
