package pawemix.chronicles.controller.editor;

import android.util.ArraySet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.checklist.DynamicFirmChecklistItem;
import pawemix.chronicles.framework.checklist.FirmChecklistItem;
import pawemix.chronicles.framework.checklist.LooseChecklistItem;
import pawemix.chronicles.framework.filter.ColumnOrder;
import pawemix.chronicles.framework.filter.Order;
import pawemix.chronicles.model.ChecklistObserver;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.ObservableChecklistProvider;
import pawemix.chronicles.model.ObservableNoteProvider;

public class SimpleCLEditorController extends SimpleEditorController implements CLEditorController, ObservableCLEditorController, ChecklistObserver {

    private final Collection<CLEditorObserver> observers = new ArrayList<>();
    private final ObservableChecklistProvider checklistProvider;

    private List<LooseChecklistItem> looses;
    private Set<DynamicFirmChecklistItem> bases;
    private Set<DynamicFirmChecklistItem> removals;

    private final Order<LooseChecklistItem> indexOrder;
    private final Order<LooseChecklistItem> checkOrder;

    public SimpleCLEditorController(ObservableNoteProvider noteProvider, ObservableChecklistProvider checklistProvider) {

        super(noteProvider);
        this.checklistProvider = checklistProvider;
        checklistProvider.addObserver(this);

        looses = new ArrayList<>();
        bases = new ArraySet<>();
        removals = new ArraySet<>();

        indexOrder = new ColumnOrder<>(new ArrayList<>(Arrays.asList(DB.CHECKLISTS_INDEX, DB.CHECKLISTS_ID)), true);
        checkOrder = new ColumnOrder<>(
                new ArrayList<>(Arrays.asList(DB.CHECKLISTS_CHECKED, DB.CHECKLISTS_INDEX, DB.CHECKLISTS_ID)), true);
    }

    @Override
    public void loadNote(Long id) {

        super.loadNote(id);

        List<FirmChecklistItem> items = checklistProvider.fetchChecklistItems(id, checkOrder.clause());
        List<DynamicFirmChecklistItem> dynamicItems = new ArrayList<>(items.size());
        for(FirmChecklistItem item : items) dynamicItems.add(new DynamicFirmChecklistItem(item));

        looses.clear();
        looses.addAll(dynamicItems);
        bases.clear();
        bases.addAll(dynamicItems);
        removals.clear();

        notifyItemsReloaded();
    }

    @Override
    public void initializeNote() {

        super.initializeNote();

        looses.clear();
        bases.clear();
        removals.clear();
        notifyItemsReloaded();
    }

    @Override
    public boolean isModified() {
        return super.isModified() || !removals.isEmpty() || firmsModified() || bases.size() != looses.size();
    }

    private boolean firmsModified() {
        boolean modifiedFirms = false;

        for(DynamicFirmChecklistItem item : bases) {
            if(item.isModified()) {
                modifiedFirms = true;
                break;
            }
        }
        return modifiedFirms;
    }

    @Override
    public boolean revert() {

        boolean noteResult = super.revert();

        if(!removals.isEmpty() || firmsModified() || bases.size() != looses.size()) {

            looses.clear();
            looses.addAll(bases);

            for(DynamicFirmChecklistItem item : bases) item.revert();

            removals.clear();

            notifyItemsReloaded();

            return true;
        }
        return noteResult;
    }

    @Override
    public boolean save() {

        boolean noteResult = super.save();

        if(!firmsModified() && removals.isEmpty() && looses.size() == bases.size()) return noteResult;

        checklistProvider.removeObserver(this);

        for(DynamicFirmChecklistItem item : removals) {
            checklistProvider.deleteChecklistItem(item.getID());
            bases.remove(item);
        }
        removals.clear();

        for(DynamicFirmChecklistItem item : bases) {
            item.save();
            checklistProvider.updateChecklistItem(item.getID(), item.toContentValues());
        }

        Collection<LooseChecklistItem> toSave = new ArrayList<>(looses);
        toSave.removeAll(bases);

        for(LooseChecklistItem item : toSave) {
            DynamicFirmChecklistItem saved = new DynamicFirmChecklistItem(checklistProvider.createChecklistItem(
                    note.getLong(DB.NOTES_ID), item.getIndex(), item.toContentValues()));
            bases.add(saved);
            looses.set(looses.indexOf(item), saved);
        }

        checklistProvider.addObserver(this);
        return true;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        checklistProvider.removeObserver(this);
    }

    @Override
    public int checklistSize() {
        return looses.size();
    }

    @Override
    public void setCheckedAt(int position, boolean b) {

        LooseChecklistItem modified = looses.get(position);

        modified.setChecked(b);

        Collections.sort(looses, checkOrder.comparator());

        int newPos = Collections.binarySearch(looses, modified, checkOrder.comparator());
        if(newPos < 0) newPos = -newPos - 1;

        if(position == newPos) notifyItemUpdated(position);
        else {
            notifyItemMoved(position, newPos);
            notifyItemUpdated(newPos);
        }
    }

    @Override
    public void setItemContent(int position, String content) {

        LooseChecklistItem modified = looses.get(position);

        modified.setContent(content);

        notifyItemUpdated(position);
    }

    @Override
    public void removeItemAt(int position) {

        LooseChecklistItem deleted = looses.get(position);

        List<LooseChecklistItem> shifted = new ArrayList<>(looses);
        Collections.sort(shifted, indexOrder.comparator());
        for(int i = shifted.indexOf(deleted); i < shifted.size(); i++)
            shifted.get(i).setIndex(shifted.get(i).getIndex() - 1);

        looses.remove(deleted);

        if(bases.contains(deleted))
            removals.add((DynamicFirmChecklistItem) deleted);

        notifyItemRemoved(position);
    }

    @Override
    public String getItemContentAt(int position) {
        return looses.get(position).getContent();
    }

    @Override
    public boolean isCheckedAt(int position) {
        return looses.get(position).isChecked();
    }

    @Override
    public void addItem() {

        LooseChecklistItem added = new LooseChecklistItem(looses.size() - 1);
        int position = Collections.binarySearch(looses, added, checkOrder.comparator());
        if(position < 0) position = -position - 1;
        looses.add(position, added);
        notifyItemInserted(position);
    }

    @Override
    public boolean swapItems(int firstPos, int secondPos) {
        LooseChecklistItem first = looses.get(firstPos);
        LooseChecklistItem second = looses.get(secondPos);

        int firstIndex = first.getIndex();
        first.setIndex(second.getIndex());
        second.setIndex(firstIndex);

        Collections.sort(looses, checkOrder.comparator());

        int newFirstPos = Collections.binarySearch(looses, first, checkOrder.comparator());
        if(newFirstPos < 0) newFirstPos = -newFirstPos - 1;

        notifyItemMoved(firstPos, newFirstPos);

        return true;
    }

    // ======== ################### ======== //
    // ======== CLEDITOR OBSERVABLE ======== //
    // ======== ################### ======== //

    @Override
    public void addObserver(CLEditorObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(CLEditorObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyItemUpdated(int position) {
        for(CLEditorObserver observer : observers) observer.onItemUpdated(position);
    }

    @Override
    public void notifyItemRemoved(int position) {
        for(CLEditorObserver observer : observers) observer.onItemRemoved(position);
    }

    @Override
    public void notifyItemInserted(int position) {
        for(CLEditorObserver observer : observers) observer.onItemInserted(position);
    }

    @Override
    public void notifyItemMoved(int from, int to) {
        for(CLEditorObserver observer : observers) observer.onItemMoved(from, to);
    }

    @Override
    public void notifyItemsReloaded() {
        for(CLEditorObserver observer : observers) observer.onItemsReloaded();
    }

    // ======== ########################### ======== //
    // ======== CHECKLIST_PROVIDER_OBSERVER ======== //
    // ======== ########################### ======== //

    @Override
    public void onChecklistItemCreated(FirmChecklistItem item) {
        if(note.contains(DB.NOTES_ID) && item.getNoteID() == note.getLong(DB.NOTES_ID)) {

            DynamicFirmChecklistItem added = new DynamicFirmChecklistItem(item);

            int position = Collections.binarySearch(looses, added, checkOrder.comparator());
            if(position < 0) position = -position - 1;
            bases.add(added);
            looses.add(position, added);

            notifyItemInserted(position);
        }
    }

    @Override
    public void onChecklistItemDeleted(long id) {
        for(Iterator<DynamicFirmChecklistItem> iterator = bases.iterator(); iterator.hasNext();) {
            DynamicFirmChecklistItem item = iterator.next();
            if(item.getID() == id) {
                int loosePosition = looses.indexOf(item);
                iterator.remove();
                removeItemAt(loosePosition);
                break;
            }
        }
    }

    @Override
    public void onChecklistItemsDeleted(List<FirmChecklistItem> deleteds) {

        if(note.contains(DB.NOTES_ID)) {

            deleteds = new ArrayList<>(deleteds);
            for(Iterator<FirmChecklistItem> iterator = deleteds.iterator(); iterator.hasNext(); )
                if(iterator.next().getNoteID() != note.getLong(DB.NOTES_ID))
                    iterator.remove();

            // w tym momencie deleteds to już lista CLItemów dotyczących wyłącznie obecnej notatki
            // FIXME odpowiednie ustawianie indexów w looses

            PriorityQueue<FirmChecklistItem> deletedCLItems = new PriorityQueue<>(
                    deleteds.size(), (first, second) -> Long.compare(first.getID(), second.getID()));
            deletedCLItems.addAll(deleteds);
            PriorityQueue<FirmChecklistItem> storedCLItems = new PriorityQueue<>(
                    bases.size(), (first, second) -> Long.compare(first.getID(), second.getID()));
            storedCLItems.addAll(bases);

            while(!deletedCLItems.isEmpty() && !storedCLItems.isEmpty()) {

                FirmChecklistItem deleted = deletedCLItems.poll();

                while(!storedCLItems.isEmpty() && Long.compare(deleted.getID(), storedCLItems.peek().getID()) > 0)
                    storedCLItems.poll();

                if(!storedCLItems.isEmpty() && Long.compare(deleted.getID(), storedCLItems.peek().getID()) == 0) {
                    FirmChecklistItem stored = storedCLItems.poll();
                    int position = looses.indexOf(stored);
                    bases.remove(stored);
                    looses.remove(stored);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    @Override
    public void onChecklistItemUpdated(long id, Bundleable updates) {
        for(FirmChecklistItem item : bases) {
            if(item.getID() == id) {
                item.updateBundle(updates.toBundle());
                notifyItemUpdated(looses.indexOf(item));
                break;
            }
        }
    }
}