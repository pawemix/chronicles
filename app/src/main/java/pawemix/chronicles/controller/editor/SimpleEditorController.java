package pawemix.chronicles.controller.editor;

import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.SimpleBundleable;
import pawemix.chronicles.framework.note.FirmNote;
import pawemix.chronicles.framework.note.LooseNote;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.NoteObserver;
import pawemix.chronicles.model.ObservableNoteProvider;

public class SimpleEditorController implements NoteObserver, EditorController, ObservableEditorController {

    protected ObservableNoteProvider provider;

    protected LooseNote note;
    protected LooseNote backup;

    private final Set<EditorObserver> observers = new HashSet<>();

    public SimpleEditorController(ObservableNoteProvider provider) {
        this.provider = provider;
        provider.addObserver(this);

        note = new LooseNote(new Bundle());

        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        note.setString(DB.NOTES_CREATED, now);
        note.setString(DB.NOTES_MODIFIED, now);
        note.setString(DB.NOTES_START, now.substring(0, 16));

        note.setString(DB.NOTES_CATEGORY, DB.DEFAULTS_CATEGORY);
        note.setString(DB.NOTES_SECTION, DB.SECTION_DEFAULT);

        backup = new LooseNote(note.toBundle());
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.provider.removeObserver(this);
        this.provider = null;
    }

    // ======== ################# ======== //
    // ======== Właściwe operacje ======== //
    // ======== ################# ======== //

    @Override
    public void loadNote(Long id) {

        note = provider.fetchNote(id);
        backup = new FirmNote(id, note.toBundle());

        notifyNoteUpdated(note);
    }

    @Override
    public void initializeNote() {

        note = new LooseNote(new Bundle());

        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        note.setString(DB.NOTES_CREATED, now);
        note.setString(DB.NOTES_MODIFIED, now);
        note.setString(DB.NOTES_START, now.substring(0, 16));

        note.setString(DB.NOTES_CATEGORY, DB.DEFAULTS_CATEGORY);
        note.setString(DB.NOTES_SECTION, DB.SECTION_DEFAULT);

        backup = new LooseNote(note.toBundle());

        notifyNoteUpdated(note);
    }

    @Override
    public String getTitle() {
        return note.getTitle();
    }

    @Override
    public String getContent() {
        return note.getContent();
    }

    @Override
    public String getStart() {
        return note.getStart();
    }

    @Override
    public String getEnd() {
        return note.getEnd();
    }

    @Override
    public String getCategory() {
        return note.getCategory();
    }

    @Override
    public String getSection() {
        return note.getSection();
    }

    @Override
    public void setTitle(String title) {
        note.setTitle(title);
        notifyNoteUpdated(note);
    }

    @Override
    public void setContent(String content) {
        note.setContent(content);
        notifyNoteUpdated(note);
    }

    @Override
    public void setStart(String start) {
        note.setStart(start);
        notifyNoteUpdated(note);
    }

    @Override
    public void setEnd(String end) {
        note.setEnd(end);
        notifyNoteUpdated(note);
    }

    @Override
    public void setCategory(String category) {
        note.setCategory(category);
        notifyNoteUpdated(note);
    }

    @Override
    public boolean save() {

        if(note.bundleEqualTo(backup.toBundle())) return false;

        else {

            provider.removeObserver(this);
            if(note.contains(DB.NOTES_ID)) provider.updateNote(note.getLong(DB.NOTES_ID), note.toContentValues());
            else note.setLong(DB.NOTES_ID, provider.createNote(note.toContentValues()));
            provider.addObserver(this);
            backup.setBundle(note.toBundle());
            return true;
        }
    }

    @Override
    public boolean revert() {
        if(note.bundleEqualTo(backup.toBundle())) return false;
        else {
            note.setBundle(backup.toBundle());
            notifyNoteUpdated(note);
            return true;
        }
    }

    @Override
    public boolean isModified() {

        Bundleable noteBundleable = new SimpleBundleable(note.toBundle());

        noteBundleable.setString(DB.NOTES_MODIFIED, backup.getModified());

        return !noteBundleable.bundleEqualTo(backup.toBundle());
    }

    @Override
    public void archiveNote() {
        if(note.contains(DB.NOTES_ID))
            provider.archiveNote(note.getLong(DB.NOTES_ID));
    }

    @Override
    public void trashNote() {
        if(note.contains(DB.NOTES_ID))
            provider.trashNote(note.getLong(DB.NOTES_ID));
    }

    // ======== #################### ======== //
    // ======== Reakcja na providera ======== //
    // ======== #################### ======== //

    @Override
    public void onNoteUpdated(FirmNote old, FirmNote updated) {

        if(Objects.equals(old.getID(), note.getLong(DB.NOTES_ID))) {

            note.setBundle(updated.toBundle());
            backup.setBundle(updated.toBundle());
            notifyNoteUpdated(note);
        }
    }

    @Override
    public void onNoteCreated(FirmNote created) {
    }

    @Override
    public void onNoteDeleted(FirmNote deleted) {
        if(deleted.getID() == note.getLong(DB.NOTES_ID)) initializeNote();
    }

    @Override
    public void onNotesUpdated(List<FirmNote> olds, List<FirmNote> updateds) {

        if(note.contains(DB.NOTES_ID)) {
            for(int i = 0; i < olds.size(); i++) {
                if(olds.get(i).getID() == note.getLong(DB.NOTES_ID)) {

                    note.setBundle(updateds.get(i).toBundle());
                    backup.setBundle(updateds.get(i).toBundle());
                    notifyNoteUpdated(note);
                    break;
                }
            }
        }
    }

    @Override
    public void onNotesCreated(List<FirmNote> createds) {
    }

    @Override
    public void onNotesDeleted(Collection<FirmNote> deleteds) {

        if(note.contains(DB.NOTES_ID)) {
            for(FirmNote deleted : deleteds) {
                if(deleted.getID() == note.getLong(DB.NOTES_ID)) {
                    initializeNote();
                    break;
                }
            }
        }
    }

    // ======== ################# ======== //
    // ======== EDITOR OBSERVABLE ======== //
    // ======== ################# ======== //

    @Override
    public void addObserver(EditorObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(EditorObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyNoteUpdated(LooseNote note) {
        for(EditorObserver observer : observers) observer.onNoteUpdated(note);
    }
}
