package pawemix.chronicles.controller.editor;

public interface CLEditorController extends EditorController {

    int checklistSize();

    void removeItemAt(int position);

    void setItemContent(int position, String content);

    String getItemContentAt(int position);

    boolean isCheckedAt(int position);

    void addItem();

    boolean swapItems(int first, int second);

    void setCheckedAt(int position, boolean b);
}
