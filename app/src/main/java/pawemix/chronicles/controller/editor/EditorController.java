package pawemix.chronicles.controller.editor;

public interface EditorController {

    void loadNote(Long id);

    void initializeNote();

    String getTitle();

    String getContent();

    void setContent(String content);

    /**
     * @return false, if there was nothing to revert
     */
    boolean revert();

    /**
     * @return false, if there was nothing to save
     */
    boolean save();

    boolean isModified();

    void setTitle(String title);

    String getCategory();

    void setCategory(String categoryName);

    void setStart(String dateStart);

    void setEnd(String dateEnd);

    String getSection();

    void archiveNote();

    void trashNote();

    String getStart();

    String getEnd();

}
