package pawemix.chronicles.controller.editor;

public interface CLEditorObserver {

    void onItemUpdated(int position);

    void onItemRemoved(int position);

    void onItemInserted(int position);

    void onItemsReloaded();

    void onItemMoved(int from, int to);
}
