package pawemix.chronicles.controller.editor;

import pawemix.chronicles.framework.note.LooseNote;

public interface ObservableEditorController extends EditorController {
    void addObserver(EditorObserver observer);

    void removeObserver(EditorObserver observer);

    void notifyNoteUpdated(LooseNote note);
}
