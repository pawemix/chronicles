package pawemix.chronicles.controller.editor;

import pawemix.chronicles.framework.note.LooseNote;

public interface EditorObserver {

    void onNoteUpdated(LooseNote note);
}
