package pawemix.chronicles.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.SimpleBundleable;
import pawemix.chronicles.framework.checklist.FirmChecklistItem;
import pawemix.chronicles.framework.note.FirmNote;

public class DatabaseProvider extends SQLiteOpenHelper
        implements ObservableNoteProvider, CategoryProvider, ObservableCategoryProvider, ObservableChecklistProvider {

    private static DatabaseProvider instance;

    private NoteProviderObservable noteObservable;
    private ObservableCategoryProvider catObservable;
    private final Set<ChecklistObserver> checklistObservers = new LinkedHashSet<>();

    public static DatabaseProvider getInstance(Context appContext) {

        if(instance == null) {
            synchronized(DatabaseProvider.class) {
                if(instance == null) instance = new DatabaseProvider(appContext);
            }
        }
        return instance;
    }

    private DatabaseProvider(Context context) {

        super(context, DB.DBNAME, null, 23);

        this.noteObservable = new RawNoteProviderObservable();
        this.catObservable = new RawObservableCategoryProvider();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String query = "CREATE TABLE " + DB.NOTES + "(" +
                DB.NOTES_ID + " INTEGER PRIMARY KEY," +
                DB.NOTES_CREATED + " TEXT NOT NULL," +
                DB.NOTES_MODIFIED + " TEXT NOT NULL," +
                DB.NOTES_TITLE + " TEXT," +
                DB.NOTES_CONTENT + " TEXT," +
                DB.NOTES_START + " TEXT," +
                DB.NOTES_END + " TEXT," +
                DB.NOTES_CATEGORY + " TEXT DEFAULT '" + DB.DEFAULTS_CATEGORY + "'," +
                DB.NOTES_SECTION + " TEXT DEFAULT '" + DB.DEFAULTS_SECTION + "'," +
                DB.NOTES_TYPE + " TEXT DEFAULT '" + DB.DEFAULTS_TYPE + "')";

        sqLiteDatabase.execSQL(query);

        query = "CREATE TABLE " + DB.CATEGORIES + "(" +
                DB.CATEGORIES_NAME + " TEXT PRIMARY KEY," +
                DB.CATEGORIES_COLORKEY + " TEXT DEFAULT '" + DB.DEFAULTS_COLORKEY + "') WITHOUT ROWID";

        sqLiteDatabase.execSQL(query);

        query = "CREATE TABLE " + DB.CHECKLISTS + " (" +
                DB.CHECKLISTS_ID + " INTEGER PRIMARY KEY," +
                DB.CHECKLISTS_NOTEID + " INTEGER NOT NULL, " +
                DB.CHECKLISTS_INDEX + " INTEGER NOT NULL," +
                DB.CHECKLISTS_CONTENT + " TEXT," +
                DB.CHECKLISTS_CHECKED + " INTEGER DEFAULT '" + DB.DEFAULTS_CHECKED + "')";

        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        String query;

        if(i < 10) { // v.10 -- kategorie ze stringowymi kluczami kolorów zamiast intowych identyfikatorów na DB.color'y
            sqLiteDatabase.beginTransaction();
            try {
                query = "CREATE TABLE temporal (" + DB.CATEGORIES_NAME + " TEXT PRIMARY KEY," +
                        DB.CATEGORIES_COLORKEY + " TEXT NOT NULL DEFAULT '" + DB.DEFAULTS_COLORKEY +
                        "') WITHOUT ROWID";
                sqLiteDatabase.execSQL(query);
                Cursor cursor = sqLiteDatabase.query(DB.CATEGORIES, new String[]{DB.CATEGORIES_NAME},
                        null, null, null, null, null
                );
                ContentValues contentValues = new ContentValues(2);
                contentValues.put(DB.CATEGORIES_COLORKEY, DB.DEFAULTS_COLORKEY);
                while(cursor.moveToNext()) {
                    contentValues.put(DB.CATEGORIES_NAME, cursor.getString(cursor.getColumnIndex(DB.CATEGORIES_NAME)));
                    sqLiteDatabase.insert("temp", null, contentValues);
                }
                cursor.close();
                query = "DROP TABLE " + DB.CATEGORIES;
                sqLiteDatabase.execSQL(query);
                query = "ALTER TABLE temporal RENAME TO " + DB.CATEGORIES;
                sqLiteDatabase.execSQL(query);
                sqLiteDatabase.setTransactionSuccessful();
            }
            catch(SQLException ignored) {
            }
            sqLiteDatabase.endTransaction();
        } // v.10 END
        if(i >= 10 && i < 11) { // v.11 -- kolumna 'sekcja' w notatkach
            sqLiteDatabase.beginTransaction();
            query = "ALTER TABLE " + DB.NOTES + " ADD COLUMN " + DB.NOTES_SECTION + " TEXT NOT NULL DEFAULT '" + DB.DEFAULTS_SECTION + "'";
            try {
                sqLiteDatabase.execSQL(query);
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(DB.NOTES_SECTION, DB.DEFAULTS_SECTION);
                sqLiteDatabase.update(DB.NOTES, contentValues, null, null);
                sqLiteDatabase.setTransactionSuccessful();
            }
            catch(SQLException ignored) {
            }
            sqLiteDatabase.endTransaction();
        } // v.11 END
        // v.12 -- ubezpieczenie kolumn notes o wartości domyślne i i nienullowalność + dateModified + type: normal|checklist
        // v.13 -- bugfix i zarazem uproszczenie zamierzeń v.12; (2018-03-23) wywalam (przynajmniej narazie) te nulle, bo komplikują
        // v.14 -- pieprzenie się z ID bo baza danych się miota bez powodu
        // v.15 -- dobra, już chyba pójdzie..
        // v.16 -- już nic nie rozumiem..
        // v.17 -- nadal nic..
        // v.18 -- (2018-03-23 17:29) ok, poczytałem, już rozumiem.
        // v.19 -- no i świetnie, nie usunąłem WITHOUT ROWID.
        // v.20 -- działaj!
        if(i < 20) {
            sqLiteDatabase.beginTransaction();
            final String TEMP = "temporal";

            query = "CREATE TABLE " + TEMP + "(" +
                    DB.NOTES_ID + " INTEGER PRIMARY KEY," +
                    DB.NOTES_CREATED + " TEXT NOT NULL," +
                    DB.NOTES_MODIFIED + " TEXT NOT NULL," +
                    DB.NOTES_TITLE + " TEXT," +
                    DB.NOTES_CONTENT + " TEXT," +
                    DB.NOTES_START + " TEXT," +
                    DB.NOTES_END + " TEXT," +
                    DB.NOTES_CATEGORY + " TEXT DEFAULT '" + DB.DEFAULTS_CATEGORY + "'," +
                    DB.NOTES_SECTION + " TEXT DEFAULT '" + DB.DEFAULTS_SECTION + "'," +
                    DB.NOTES_TYPE + " TEXT DEFAULT '" + DB.DEFAULTS_TYPE + "')";

            sqLiteDatabase.execSQL(query);

            Cursor cursor = sqLiteDatabase.query(DB.NOTES, null, null, null, null, null, null);
            int startRowCount = cursor.getCount();
            int endRowCount = 0;
            ContentValues contentValues = new ContentValues();

            while(cursor.moveToNext()) {
                contentValues.clear();
                for(String column : cursor.getColumnNames()) {
                    if(column.equalsIgnoreCase(DB.NOTES_ID))
                        contentValues.put(column, cursor.getLong(cursor.getColumnIndex(column)));
                    else if(cursor.getString(cursor.getColumnIndex(column)) != null && !cursor.getString(
                            cursor.getColumnIndex(column)).isEmpty())
                        contentValues.put(column, cursor.getString(cursor.getColumnIndex(column)));
                }
                if(!contentValues.containsKey(DB.NOTES_MODIFIED) || contentValues.getAsString(
                        DB.NOTES_MODIFIED).isEmpty())
                    contentValues.put(
                            DB.NOTES_MODIFIED,
                            cursor.getString(cursor.getColumnIndex(DB.NOTES_CREATED))
                    );
                if(sqLiteDatabase.insert(TEMP, null, contentValues) != -1) endRowCount++;
            }

            cursor.close();

            if(startRowCount == endRowCount) {
                query = "DROP TABLE " + DB.NOTES;
                sqLiteDatabase.execSQL(query);
                query = "ALTER TABLE " + TEMP + " RENAME TO " + DB.NOTES;
                sqLiteDatabase.execSQL(query);
                sqLiteDatabase.setTransactionSuccessful();
            }

            sqLiteDatabase.endTransaction();
        } // v.18 END

        // v.21 - [2018-06-25 22:45] tabela checklist
        // v.22 - [2018-06-26 02:23] noteID powinno *raczej* być NOT NULL
        if(i < 22) {
            query = "CREATE TABLE " + DB.CHECKLISTS + "(" +
                    DB.CHECKLISTS_ID + " INTEGER PRIMARY KEY," +
                    DB.CHECKLISTS_CONTENT + " TEXT," +
                    DB.CHECKLISTS_CHECKED + " INTEGER NOT NULL DEFAULT '" + DB.DEFAULTS_CHECKED + "'," +
                    DB.CHECKLISTS_NOTEID + " INTEGER NOT NULL)";

            sqLiteDatabase.execSQL(query);
        }
        // v.22 END

        // v.23 [2018-07-12 16:39] checklisty mają teraz porządek użytkownika
        if(i < 23) {

            query = "CREATE TABLE temporal (" +
                    DB.CHECKLISTS_ID + " INTEGER PRIMARY KEY, " +
                    DB.CHECKLISTS_NOTEID + " INTEGER NOT NULL, " +
                    DB.CHECKLISTS_INDEX + " INTEGER NOT NULL, " +
                    DB.CHECKLISTS_CONTENT + " TEXT, " +
                    DB.CHECKLISTS_CHECKED + " INTEGER DEFAULT '" + DB.DEFAULTS_CHECKED + "')";

            sqLiteDatabase.execSQL(query);

            query = "INSERT INTO temporal (" +
                    DB.CHECKLISTS_ID + ", " +
                    DB.CHECKLISTS_NOTEID + ", " +
                    DB.CHECKLISTS_INDEX + ", " +
                    DB.CHECKLISTS_CONTENT + ", " +
                    DB.CHECKLISTS_CHECKED + ") " +
                    "SELECT " +
                    DB.CHECKLISTS_ID + ", " +
                    DB.CHECKLISTS_NOTEID + ", " +
                    "(SELECT COUNT(*) FROM " + DB.CHECKLISTS + " c " +
                    "WHERE c." + DB.CHECKLISTS_NOTEID + " = " + DB.CHECKLISTS + "." + DB.CHECKLISTS_NOTEID +
                    " AND c." + DB.CHECKLISTS_ID + " < " + DB.CHECKLISTS + "." + DB.CHECKLISTS_ID + "), " +
                    DB.CHECKLISTS_CONTENT + ", " + DB.CHECKLISTS_CHECKED + " FROM " + DB.CHECKLISTS;

            sqLiteDatabase.execSQL(query);

            query = "DROP TABLE " + DB.CHECKLISTS;

            sqLiteDatabase.execSQL(query);

            query = "ALTER TABLE temporal RENAME TO " + DB.CHECKLISTS;

            sqLiteDatabase.execSQL(query);
        }
    }

    // ======== ################################### ======== //
    // ======== podstawowe metody interakcji z bazą ======== //
    // ======== ################################### ======== //

    @Override
    public Bundle fetchCategory(String name) {

        Cursor cursor = getReadableDatabase().query(
                DB.CATEGORIES, null, DB.CATEGORIES_NAME + " LIKE ?",
                new String[]{name}, null, null, null
        );
        if(cursor.moveToNext()) {
            String key = cursor.getString(cursor.getColumnIndex(DB.CATEGORIES_COLORKEY));
            Bundle bundle = new Bundle(2);
            bundle.putString(DB.CATEGORIES_NAME, name);
            bundle.putString(DB.CATEGORIES_COLORKEY, key);
            cursor.close();
            return bundle;
        }
        cursor.close();
        return null;
    }

    @Override
    public List<Bundle> fetchCategories() {

        Cursor cursor = getReadableDatabase().query(
                DB.CATEGORIES, null, null, null, null, null, DB.CATEGORIES_NAME + " ASC");
        List<Bundle> result = new ArrayList<>(cursor.getCount());
        Bundle bundle;
        while(cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(DB.CATEGORIES_NAME));
            String key = cursor.getString(cursor.getColumnIndex(DB.CATEGORIES_COLORKEY));
            bundle = new Bundle(2);
            bundle.putString(DB.CATEGORIES_NAME, name);
            bundle.putString(DB.CATEGORIES_COLORKEY, key);
            result.add(bundle);
        }
        cursor.close();
        return result;
    }

    @Override
    public void setCategoryName(String oldName, String newName) {

        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(DB.CATEGORIES_NAME, newName);
        database.beginTransaction();
        if(database.update(DB.CATEGORIES, contentValues, DB.CATEGORIES_NAME + " LIKE ?", new String[]{oldName}) == 1) {
            contentValues.clear();
            contentValues.put(DB.NOTES_CATEGORY, newName);
            if(updateNotes(DB.NOTES_CATEGORY + " LIKE ?", new String[]{oldName}, contentValues)) {
                database.setTransactionSuccessful();

                notifyCategoryUpdated(oldName, fetchCategory(newName));
            }
        }
        database.endTransaction();
    }

    @Override
    public void deleteCategory(String categoryName, String surrogateName) {

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        int deleteResult = database.delete(DB.CATEGORIES, DB.CATEGORIES_NAME + " LIKE ?", new String[]{categoryName});

        ContentValues surrogateValues = new ContentValues(1);
        surrogateValues.put(DB.NOTES_CATEGORY, surrogateName);

        List<FirmNote> oldNotes = fetchNotes(DB.NOTES_CATEGORY + " LIKE ?", new String[]{categoryName}, null);

        int updateResult = database.update(
                DB.NOTES, surrogateValues, DB.NOTES_CATEGORY + " LIKE ?", new String[]{categoryName});
        if(deleteResult == 1 && updateResult == oldNotes.size()) {
            database.setTransactionSuccessful();

            Bundle deletedCategory = new Bundle(2);
            deletedCategory.putString(DB.CATEGORIES_NAME, categoryName);
            deletedCategory.putString(DB.CATEGORIES_COLORKEY, DB.DEFAULTS_COLORKEY);

            catObservable.notifyCategoryDeleted(deletedCategory);

            List<FirmNote> updatedNotes = new ArrayList<>(oldNotes);
            for(FirmNote note : updatedNotes) note.setString(DB.NOTES_CATEGORY, surrogateName);

            noteObservable.notifyNotesUpdated(oldNotes, updatedNotes);
        }
        database.endTransaction();
    }

    @Override
    public void addCategory(String categoryName, String colorKey) {

        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(DB.CATEGORIES_NAME, categoryName);
        contentValues.put(DB.CATEGORIES_COLORKEY, colorKey);
        database.beginTransaction();

        if(database.insert(DB.CATEGORIES, null, contentValues) != -1) {

            database.setTransactionSuccessful();
            database.endTransaction();

            Bundle createdCategory = new Bundle(2);
            createdCategory.putString(DB.CATEGORIES_NAME, categoryName);
            createdCategory.putString(DB.CATEGORIES_COLORKEY, colorKey);

            catObservable.notifyCategoryCreated(createdCategory);
        }

        else database.endTransaction();
    }

    @Override
    public boolean categoryHasNotes(String name) {

        SQLiteDatabase database = getWritableDatabase();
        Cursor cursor = database.query(DB.NOTES, new String[]{DB.NOTES_ID},
                DB.NOTES_CATEGORY + " LIKE ?", new String[]{name}, null, null, null
        );
        if(cursor.getCount() > 0) {
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }

    @Override
    public void setColorKey(String categoryName, String colorKey) {

        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(DB.CATEGORIES_COLORKEY, colorKey);
        database.beginTransaction();
        if(database.update(DB.CATEGORIES, contentValues, DB.CATEGORIES_NAME + " LIKE ?",
                new String[]{categoryName}
        ) == 1) {
            database.setTransactionSuccessful();

            Bundle updatedCategory = new Bundle(2);

            updatedCategory.putString(DB.CATEGORIES_NAME, categoryName);
            updatedCategory.putString(DB.CATEGORIES_COLORKEY, colorKey);

            notifyCategoryUpdated(categoryName, updatedCategory);
        }
        database.endTransaction();
    }

    /**
     * @param whereClause brak filtra WHERE, jeśli null
     * @param whereArgs   tabela takiej długości, ile '?' umieszczono w filtrze WHERE
     * @param orderClause brak filtra porządku ORDER, jeśli null, skutkuje zwróceniem danych w porządku takim, jaki jest w bazie
     */
    @Override
    public List<FirmNote> fetchNotes(String whereClause, String[] whereArgs, String orderClause) {

        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(DB.NOTES, null, whereClause, whereArgs, null, null, orderClause);

        List<FirmNote> notes = new ArrayList<>(cursor.getCount());

        int columnCount = cursor.getColumnCount();

        Bundle bundle;

        while(cursor.moveToNext()) {

            bundle = new Bundle(columnCount);

            for(String column : cursor.getColumnNames())
                if(!cursor.isNull(cursor.getColumnIndex(column)))
                    bundle.putString(column, cursor.getString(cursor.getColumnIndex(column)));

            notes.add(new FirmNote(Long.parseLong(bundle.getString(DB.NOTES_ID)), bundle));
        }
        cursor.close();

        return notes;
    }

    @Override
    public boolean deleteNote(long id) {

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        FirmNote noteToDelete = fetchNote(id);
        boolean outcome;
        if(outcome = (database.delete(DB.NOTES, DB.NOTES_ID + " = ?", new String[]{String.valueOf(id)}) == 1)) {

            List<FirmChecklistItem> deletedCLItems = fetchChecklistItems(id, null);

            database.delete(DB.CHECKLISTS, DB.CHECKLISTS_NOTEID + " = ?", new String[]{String.valueOf(id)});

            database.setTransactionSuccessful();

            noteObservable.notifyNoteDeleted(noteToDelete);
            notifyChecklistItemsDeleted(deletedCLItems);
        }
        database.endTransaction();

        return outcome;
    }

    @Override
    public void deleteNotes(Collection<Long> ids) {

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        String whereClause = DB.NOTES_ID + " IN " + ids.toString()
                .replace('[', '(')
                .replace(']', ')')
                .replaceAll("\\d+", "?");

        String[] whereArgs = new String[ids.size()];
        int index = 0;
        for(Long id : ids) whereArgs[index++] = id.toString();

        List<FirmNote> notesToDelete = fetchNotes(whereClause, whereArgs, null);

        if(database.delete(DB.NOTES, whereClause, whereArgs) == ids.size()) {

            whereClause = DB.CHECKLISTS_NOTEID + " IN " + ids.toString()
                    .replace('[', '(')
                    .replace(']', ')')
                    .replaceAll("\\d", "?");

            List<FirmChecklistItem> clitemsToDelete = fetchChecklistItems(whereClause, whereArgs, null);

            if(clitemsToDelete.size() > 0) {
                if(database.delete(DB.CHECKLISTS, whereClause, whereArgs) == clitemsToDelete.size()) {

                    database.setTransactionSuccessful();
                    notifyNotesDeleted(notesToDelete);
                    notifyChecklistItemsDeleted(clitemsToDelete);
                }
            }

            else {

                database.setTransactionSuccessful();
                notifyNotesDeleted(notesToDelete);
            }
        }
        database.endTransaction();
    }

    @Override
    public FirmNote fetchNote(long id) {

        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(DB.NOTES, null, DB.NOTES_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null
        );

        Bundle bundle = null;

        if(cursor.moveToNext()) {
            bundle = new Bundle(cursor.getColumnCount());
            for(String column : cursor.getColumnNames())
                if(!cursor.isNull(cursor.getColumnIndex(column)))
                    bundle.putString(column, cursor.getString(cursor.getColumnIndex(column)));
        }

        cursor.close();

        return bundle != null? new FirmNote(Long.parseLong(bundle.getString(DB.NOTES_ID)), bundle) : null;
    }

    @Override
    public List<String> getNoteColumns() {

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(DB.NOTES, null, null, null, null, null, null, "1");
        List<String> noteColumns = new ArrayList<>(cursor.getColumnCount());
        noteColumns.addAll(Arrays.asList(cursor.getColumnNames()));
        cursor.close();
        return noteColumns;
    }

    @Override
    public String getNoteColumnAt(int position) {
        return getNoteColumns().get(position);
    }

    @Override
    public Long createNote(ContentValues values) {

        SQLiteDatabase database = getWritableDatabase();

        if(!values.containsKey(DB.NOTES_CREATED)) {
            if(!values.containsKey(DB.NOTES_MODIFIED)) {
                String now = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
                values.put(DB.NOTES_CREATED, now);
                values.put(DB.NOTES_MODIFIED, now);
            }
            else values.put(DB.NOTES_CREATED, values.getAsString(DB.NOTES_MODIFIED));
        }
        else if(!values.containsKey(DB.NOTES_MODIFIED))
            values.put(DB.NOTES_MODIFIED, values.getAsString(DB.NOTES_CREATED));

        database.beginTransaction();
        Long id;
        if((id = database.insert(DB.NOTES, null, values)) > 0) {
            Bundle bundle = new Bundle(values.size() + 1);
            bundle.putLong(DB.NOTES_ID, id);
            for(String key : values.keySet()) bundle.putString(key, values.getAsString(key));

            database.setTransactionSuccessful();
            database.endTransaction();
            noteObservable.notifyNoteCreated(new FirmNote(id, bundle));
        }
        else database.endTransaction();
        return id;
    }

    @Override
    public void updateNote(Long id, ContentValues changedValues) {

        SQLiteDatabase database = getWritableDatabase();

        database.beginTransaction();

        FirmNote old = fetchNote(id);

        if(database.update(DB.NOTES, changedValues, DB.NOTES_ID + " = ?", new String[]{id.toString()}) == 1) {

            database.setTransactionSuccessful();

            FirmNote updated = fetchNote(id);

            noteObservable.notifyNoteUpdated(old, updated);
        }
        database.endTransaction();
    }

    @Override
    public boolean updateNotes(String whereClause, String[] whereArgs, ContentValues changedValues) {

        List<FirmNote> olds = fetchNotes(whereClause, whereArgs, null);

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        boolean result;

        if(result = (database.update(DB.NOTES, changedValues, whereClause, whereArgs) == olds.size())) {
            database.setTransactionSuccessful();

            List<FirmNote> updateds = new ArrayList<>(olds.size());
            for(FirmNote old : olds) {
                FirmNote updated = new FirmNote(old.getID(), old.toBundle());
                updated.updateBundle(changedValues);
                updateds.add(updated);
            }

            noteObservable.notifyNotesUpdated(olds, updateds);
        }
        database.endTransaction();
        return result;
    }

    // ======== ########################### ======== //
    // ======== metody wyższe abstrakcyjnie ======== //
    // ======== ########################### ======== //

    @Override
    public void restoreNote(Long id) {

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(DB.NOTES_SECTION, DB.SECTION_DEFAULT);
        updateNote(id, contentValues);
    }

    @Override
    public void archiveNote(Long id) {

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(DB.NOTES_SECTION, DB.SECTION_ARCHIVE);
        updateNote(id, contentValues);
    }

    @Override
    public void trashNote(Long id) {

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(DB.NOTES_SECTION, DB.SECTION_TRASHBIN);
        updateNote(id, contentValues);
    }

    @Override
    public void emptyTrashbin() {

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        List<FirmNote> notesToDelete = fetchNotes(
                DB.NOTES_SECTION + " LIKE ?", new String[]{DB.SECTION_TRASHBIN}, null);
        if(database.delete(DB.NOTES, DB.NOTES_SECTION + " LIKE ?",
                new String[]{DB.SECTION_TRASHBIN}
        ) == notesToDelete.size()) {

            database.setTransactionSuccessful();
            database.endTransaction();
            notifyNotesDeleted(notesToDelete);
        }
    }

    // ======== ############################# ======== //
    // ======== OBSERVABLE_CHECKLIST_PROVIDER ======== //
    // ======== ############################# ======== //

    @Override
    public void addObserver(ChecklistObserver observer) {
        checklistObservers.add(observer);
    }

    @Override
    public void removeObserver(ChecklistObserver observer) {
        checklistObservers.remove(observer);
    }

    @Override
    public FirmChecklistItem createChecklistItem(long noteID, int index, ContentValues values) {

        if(noteID < 0) return null;

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        values.put(DB.CHECKLISTS_NOTEID, noteID);
        values.put(DB.CHECKLISTS_INDEX, index);

        long id = database.insert(DB.CHECKLISTS, null, values);

        if(id > -1) {

            database.setTransactionSuccessful();
            database.endTransaction();
            FirmChecklistItem added = new FirmChecklistItem(
                    id, noteID, index, SimpleBundleable.buildBundle(values, Collections.emptySet()));
            notifyChecklistItemCreated(added);
            return added;
        }

        database.endTransaction();
        return null;
    }

    @Override
    public void updateChecklistItem(long id, ContentValues updates) {

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        if(database.update(DB.CHECKLISTS, updates, DB.CHECKLISTS_ID + " = ?", new String[]{String.valueOf(id)}) == 1) {

            database.setTransactionSuccessful();
            database.endTransaction();
            notifyChecklistItemUpdated(
                    id, new SimpleBundleable(SimpleBundleable.buildBundle(updates, Collections.emptySet())));
            return;
        }

        database.endTransaction();
    }

    @Override
    public void deleteChecklistItem(long id) {

        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();

        if(database.delete(DB.CHECKLISTS, DB.CHECKLISTS_ID + " = ?", new String[]{String.valueOf(id)}) == 1) {

            database.setTransactionSuccessful();
            database.endTransaction();
            notifyChecklistItemDeleted(id);
            return;
        }

        database.endTransaction();
    }

    @Override
    public List<FirmChecklistItem> fetchChecklistItems(long noteID, String orderClause) {

        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(DB.CHECKLISTS, null, DB.CHECKLISTS_NOTEID + " = ?",
                new String[]{String.valueOf(noteID)}, null, null, orderClause
        );

        List<FirmChecklistItem> items = new ArrayList<>(cursor.getCount());

        while(cursor.moveToNext()) {

            Bundle bundle = new Bundle(cursor.getColumnCount());

            long id = cursor.getLong(cursor.getColumnIndex(DB.CHECKLISTS_ID));
            int index = cursor.getInt(cursor.getColumnIndex(DB.CHECKLISTS_INDEX));

            bundle.putLong(DB.CHECKLISTS_ID, id);
            bundle.putLong(DB.CHECKLISTS_NOTEID, noteID);
            bundle.putInt(DB.CHECKLISTS_INDEX, index);

            bundle.putInt(DB.CHECKLISTS_CHECKED, cursor.getInt(cursor.getColumnIndex(DB.CHECKLISTS_CHECKED)));
            bundle.putString(DB.CHECKLISTS_CONTENT, cursor.getString(cursor.getColumnIndex(DB.CHECKLISTS_CONTENT)));

            items.add(new FirmChecklistItem(id, noteID, index, bundle));
        }

        cursor.close();
        return items;
    }

    @Override
    public List<FirmChecklistItem> fetchChecklistItems(String whereClause, String[] whereArgs, String orderClause) {

        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(DB.CHECKLISTS, null, whereClause, whereArgs, null, null, orderClause);

        List<FirmChecklistItem> items = new ArrayList<>(cursor.getCount());

        while(cursor.moveToNext()) {

            Bundle bundle = new Bundle(cursor.getColumnCount());

            long id = cursor.getLong(cursor.getColumnIndex(DB.CHECKLISTS_ID));
            long noteID = cursor.getLong(cursor.getColumnIndex(DB.CHECKLISTS_NOTEID));
            int index = cursor.getInt(cursor.getColumnIndex(DB.CHECKLISTS_INDEX));

            bundle.putLong(DB.CHECKLISTS_ID, id);
            bundle.putLong(DB.CHECKLISTS_NOTEID, noteID);
            bundle.putInt(DB.CHECKLISTS_INDEX, index);

            bundle.putInt(DB.CHECKLISTS_CHECKED, cursor.getInt(cursor.getColumnIndex(DB.CHECKLISTS_CHECKED)));
            bundle.putString(DB.CHECKLISTS_CONTENT, cursor.getString(cursor.getColumnIndex(DB.CHECKLISTS_CONTENT)));

            items.add(new FirmChecklistItem(id, noteID, index, bundle));
        }

        cursor.close();
        return items;
    }

    @Override
    public void notifyChecklistItemCreated(FirmChecklistItem item) {
        for(ChecklistObserver observer : checklistObservers) observer.onChecklistItemCreated(item);
    }

    @Override
    public void notifyChecklistItemUpdated(long id, Bundleable updates) {
        for(ChecklistObserver observer : checklistObservers) observer.onChecklistItemUpdated(id, updates);
    }

    @Override
    public void notifyChecklistItemDeleted(long id) {
        for(ChecklistObserver observer : checklistObservers) observer.onChecklistItemDeleted(id);
    }

    @Override
    public void notifyChecklistItemsDeleted(List<FirmChecklistItem> deleteds) {
        for(ChecklistObserver observer : checklistObservers) observer.onChecklistItemsDeleted(deleteds);
    }

    // ======== ########################## ======== //
    // ======== ObservableCategoryProvider ======== //
    // ======== ########################## ======== //

    @Override
    public void addObserver(CategoryObserver observer) {
        catObservable.addObserver(observer);
    }

    @Override
    public void removeObserver(CategoryObserver observer) {
        catObservable.removeObserver(observer);
    }

    @Override
    public void notifyCategoryUpdated(String oldName, Bundle updatedCategory) {
        catObservable.notifyCategoryUpdated(oldName, updatedCategory);
    }

    @Override
    public void notifyCategoryDeleted(Bundle deletedCategory) {
        catObservable.notifyCategoryDeleted(deletedCategory);
    }

    @Override
    public void notifyCategoryCreated(Bundle createdCategory) {
        catObservable.notifyCategoryCreated(createdCategory);
    }

    // ======== ###################### ======== //
    // ======== NoteProviderObservable ======== //
    // ======== ###################### ======== //

    @Override
    public void addObserver(NoteObserver observer) {
        noteObservable.addObserver(observer);
    }

    @Override
    public void removeObserver(NoteObserver observer) {
        noteObservable.removeObserver(observer);
    }

    @Override
    public void notifyNoteUpdated(FirmNote old, FirmNote updated) {
        noteObservable.notifyNoteUpdated(old, updated);
    }

    @Override
    public void notifyNotesUpdated(List<FirmNote> olds, List<FirmNote> updateds) {
        noteObservable.notifyNotesUpdated(olds, updateds);
    }

    @Override
    public void notifyNoteCreated(FirmNote created) {
        noteObservable.notifyNoteCreated(created);
    }

    @Override
    public void notifyNoteDeleted(FirmNote deleted) {
        noteObservable.notifyNoteDeleted(deleted);
    }

    @Override
    public void notifyNotesDeleted(List<FirmNote> deleteds) {
        noteObservable.notifyNotesDeleted(deleteds);
    }
}