package pawemix.chronicles.model;

import java.util.Collection;
import java.util.List;

import pawemix.chronicles.framework.note.FirmNote;

public interface NoteObserver {
    void onNoteUpdated(FirmNote old, FirmNote updated);
    void onNoteCreated(FirmNote created);
    void onNoteDeleted(FirmNote deleted);

    void onNotesUpdated(List<FirmNote> olds, List<FirmNote> updateds);
    void onNotesCreated(List<FirmNote> createds);
    void onNotesDeleted(Collection<FirmNote> deleteds);
}
