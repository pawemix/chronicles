package pawemix.chronicles.model;

import android.content.ContentValues;

import java.util.Collection;
import java.util.List;

import pawemix.chronicles.framework.note.FirmNote;

/**
 * Interfejs reprezentujący komponent aplikacji odpowiedzialny za dostarczenie NOTATEK
 * z ZEWNĘTRZNEGO ŹRÓDŁA (pliku, bazy danych).
 */
public interface NoteProvider {

    List<FirmNote> fetchNotes(String whereClause, String[] whereArgs, String orderClause);

    boolean deleteNote(long id);

    FirmNote fetchNote(long id);

    List<String> getNoteColumns();

    String getNoteColumnAt(int position);

    Long createNote(ContentValues values);

    void updateNote(Long id, ContentValues changedValues);

    boolean updateNotes(String whereClause, String[] whereArgs, ContentValues changedValues);

    void restoreNote(Long id);

    void archiveNote(Long id);

    void trashNote(Long id);

    void emptyTrashbin();

    void deleteNotes(Collection<Long> ids);

}
