package pawemix.chronicles.model;

import java.util.List;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.checklist.FirmChecklistItem;

public interface ChecklistObserver {

    void onChecklistItemCreated(FirmChecklistItem item);

    void onChecklistItemUpdated(long id, Bundleable content);

    void onChecklistItemDeleted(long id);

    void onChecklistItemsDeleted(List<FirmChecklistItem> deleteds);
}
