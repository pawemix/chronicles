package pawemix.chronicles.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import pawemix.chronicles.R;
import pawemix.chronicles.view.assistant.NoteAssistantService;

public class SimplePreferencesProvider implements PreferencesProvider {

    private static SimplePreferencesProvider instance;
    private SharedPreferences preferences;

    private final Context context;

    private SimplePreferencesProvider(Context context) {
        this.context = context;

        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);

        preferences.registerOnSharedPreferenceChangeListener((sharedPreferences, s) -> {
            if(s.equalsIgnoreCase(context.getString(R.string.prefkeys_noteassistant))) {
                Intent noteAssistantService = new Intent(
                        Intent.ACTION_MAIN, null, context, NoteAssistantService.class);
                if(sharedPreferences.getBoolean(s, false)) {
                    context.startService(noteAssistantService);
                }
                else context.stopService(noteAssistantService);
            }
        });
    }

    public static SimplePreferencesProvider getInstance(Context appContext) {

        if(instance == null) {
            synchronized(SimplePreferencesProvider.class) {
                if(instance == null) {
                    instance = new SimplePreferencesProvider(appContext);
                }
            }
        }
        return instance;
    }

    @Override
    public Pattern preferredRegex() {

        Pattern output;

        try {

            output = Pattern.compile(preferences.getString(
                    context.getString(R.string.prefkeys_filterPattern),
                    context.getString(R.string.defaults_filterpattern)
            ), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE | Pattern.DOTALL);
        }

        catch(PatternSyntaxException e) {

            output = Pattern.compile(
                    ".*", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE | Pattern.DOTALL);
        }

        return output;
    }

    @Override
    public List<Boolean> preferredAscendings() {

        String value = preferences.getString(context.getString(R.string.prefkeys_orderascending), "");

        if(value.matches("\\[(\\s*\\w+\\s*,)*\\s*\\w+\\s*\\]")) {

            value = value.substring(0, value.length() - 1).replaceAll("\\s+", "");
            String[] words = value.split(",");
            Boolean[] ascendings = new Boolean[words.length];
            for(int i = 0; i < words.length; i++) ascendings[i] = Boolean.parseBoolean(words[i]);
            return Arrays.asList(ascendings);
        }

        return defaultAscendings();
    }

    public static List<Boolean> defaultAscendings() {
        return Collections.nCopies(9, true);
    }

    @Override
    public List<String> preferredColumnOrder() {

        String value = preferences.getString(context.getString(R.string.prefkeys_ordercolumns), "");

        if(value.matches("\\[(\\s*\\w+\\s*,)*\\s*\\w+\\s*\\]")) {

            value = value.substring(0, value.length() - 1).replaceAll("\\s+", "");
            String[] columns = value.split(",");
            return Arrays.asList(columns);
        }

        return defaultColumnOrder();
    }

    public static List<String> defaultColumnOrder() {
        return Arrays.asList(
                DB.NOTES_START, DB.NOTES_END, DB.NOTES_CREATED, DB.NOTES_MODIFIED, DB.NOTES_TITLE, DB.NOTES_CONTENT,
                DB.NOTES_CATEGORY, DB.NOTES_SECTION, DB.NOTES_ID
        );
    }

    @Override
    public List<String> preferredVisibleCategories() {
        return new ArrayList<>(Collections.singletonList(DB.DEFAULTS_CATEGORY)); // TODO
    }

    @Override
    public String[] getColorKeys() {
        return context.getResources().getStringArray(R.array.colorkeys);
    }

    @Override
    public boolean assistantOnStartup() {
        return preferences.getBoolean(context.getString(R.string.prefkeys_noteassistant), false);
    }

    public static int getDarkOf(String colorKey) {
        try {
            Class r_color = R.color.class;
            Field r_color_searchedDark = r_color.getDeclaredField(colorKey + "Dark");
            return r_color_searchedDark.getInt(R.color.class);
        }
        catch(NoSuchFieldException | IllegalAccessException e) {
            return R.color.primaryDark;
        }
    }

    public static int getLightOf(String colorKey) {
        try {
            Class r_color = R.color.class;
            Field r_color_searchedLight = r_color.getDeclaredField(colorKey + "Light");
            return r_color_searchedLight.getInt(R.color.class);
        }
        catch(NoSuchFieldException | IllegalAccessException e) {
            return R.color.primary;
        }
    }
}
