package pawemix.chronicles.model;

public interface DB {

    String CHECKLISTS = "checklists";

    String CHECKLISTS_NOTEID = "noteID";

    String CHECKLISTS_ID = "ID";

    String CHECKLISTS_CONTENT = "content";

    String CHECKLISTS_CHECKED = "checked";

    String DEFAULTS_CHECKED = "0";

    String NOTES = "notes";

    String NOTES_ID = "ID";

    String NOTES_TITLE = "title";

    String NOTES_CONTENT = "content";

    String NOTES_CREATED = "dateCreated";

    String NOTES_MODIFIED = "dateModified";

    String NOTES_START = "dateStart";

    String NOTES_END = "dateEnd";

    String NOTES_CATEGORY = "noteCategory";

    String NOTES_SECTION = "section";

    String SECTION_DEFAULT = "default";

    String SECTION_ARCHIVE = "archive";

    String SECTION_TRASHBIN = "trashbin";

    String DEFAULTS_CATEGORY = "default";

    String DEFAULTS_COLORKEY = "primary";

    String NOTES_TYPE = "type";

    String CATEGORIES = "categories";

    String CATEGORIES_NAME = "name";

    String CATEGORIES_COLORKEY = "colorKey";

    String DBNAME = "chronicles.db";

    String DEFAULTS_SECTION = SECTION_DEFAULT;

    String DEFAULTS_TYPE = "normal";

    String CHECKLISTS_INDEX = "clIndex";
}
