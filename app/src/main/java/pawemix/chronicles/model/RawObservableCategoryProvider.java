package pawemix.chronicles.model;

import android.os.Bundle;

import java.util.HashSet;
import java.util.Set;

public class RawObservableCategoryProvider implements ObservableCategoryProvider {
    private Set<CategoryObserver> observers = new HashSet<>();

    @Override
    public void addObserver(CategoryObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(CategoryObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyCategoryUpdated(String oldName, Bundle updatedCategory) {
        for(CategoryObserver observer : observers) observer.onCategoryUpdated(oldName, updatedCategory);
    }

    @Override
    public void notifyCategoryDeleted(Bundle deletedCategory) {
        for(CategoryObserver observer : observers) observer.onCategoryDeleted(deletedCategory);
    }

    @Override
    public void notifyCategoryCreated(Bundle createdCategory) {
        for(CategoryObserver observer: observers) observer.onCategoryCreated(createdCategory);
    }
}
