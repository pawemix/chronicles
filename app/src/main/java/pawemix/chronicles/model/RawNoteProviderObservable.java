package pawemix.chronicles.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pawemix.chronicles.framework.note.FirmNote;

public class RawNoteProviderObservable implements NoteProviderObservable {
    private Set<NoteObserver> observers = new HashSet<>();

    @Override
    public void addObserver(NoteObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(NoteObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyNoteUpdated(FirmNote old, FirmNote updated) {
        for(NoteObserver observer : observers) observer.onNoteUpdated(old, updated);
    }

    @Override
    public void notifyNotesUpdated(List<FirmNote> olds, List<FirmNote> updateds) {
        for(NoteObserver observer : observers) observer.onNotesUpdated(olds, updateds);
    }

    @Override
    public void notifyNoteCreated(FirmNote created) {
        for(NoteObserver observer : observers) observer.onNoteCreated(created);
    }

    @Override
    public void notifyNoteDeleted(FirmNote deleted) {
        for(NoteObserver observer : observers) observer.onNoteDeleted(deleted);
    }

    @Override
    public void notifyNotesDeleted(List<FirmNote> deleteds) {
        for(NoteObserver observer : observers) observer.onNotesDeleted(deleteds);
    }
}
