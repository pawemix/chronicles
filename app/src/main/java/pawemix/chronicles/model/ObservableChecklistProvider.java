package pawemix.chronicles.model;

import java.util.List;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.framework.checklist.FirmChecklistItem;

public interface ObservableChecklistProvider extends ChecklistProvider {

    void notifyChecklistItemCreated(FirmChecklistItem item);

    void notifyChecklistItemUpdated(long id, Bundleable updates);

    void notifyChecklistItemDeleted(long id);

    void notifyChecklistItemsDeleted(List<FirmChecklistItem> deleteds);

    void addObserver(ChecklistObserver observer);

    void removeObserver(ChecklistObserver observer);
}
