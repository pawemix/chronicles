package pawemix.chronicles.model;

import android.os.Bundle;

public interface ObservableCategoryProvider {
    void addObserver(CategoryObserver observer);
    void removeObserver(CategoryObserver observer);

    void notifyCategoryUpdated(String oldName, Bundle updatedCategory);
    void notifyCategoryDeleted(Bundle deletedCategory);
    void notifyCategoryCreated(Bundle createdCategory);
}
