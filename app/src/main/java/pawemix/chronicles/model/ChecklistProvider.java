package pawemix.chronicles.model;

import android.content.ContentValues;

import java.util.List;

import pawemix.chronicles.framework.checklist.FirmChecklistItem;

public interface ChecklistProvider {

    List<FirmChecklistItem> fetchChecklistItems(String whereClause, String[] whereArgs, String orderClause);

    void deleteChecklistItem(long id);

    List<FirmChecklistItem> fetchChecklistItems(long noteID, String orderClause);

    void updateChecklistItem(long id, ContentValues values);

    FirmChecklistItem createChecklistItem(long noteID, int index, ContentValues values);
}
