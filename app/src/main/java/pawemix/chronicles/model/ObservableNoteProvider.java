package pawemix.chronicles.model;

public interface ObservableNoteProvider extends NoteProvider, NoteProviderObservable {}
