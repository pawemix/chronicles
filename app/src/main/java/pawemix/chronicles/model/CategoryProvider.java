package pawemix.chronicles.model;

import android.os.Bundle;

import java.util.List;

public interface CategoryProvider {

    Bundle fetchCategory(String name);

    List<Bundle> fetchCategories();

    void setCategoryName(String oldName, String newName);

    void deleteCategory(String categoryName, String surrogateName);

    void addCategory(String categoryName, String colorKey);

    void setColorKey(String categoryName, String colorKey);

    boolean categoryHasNotes(String name);
}
