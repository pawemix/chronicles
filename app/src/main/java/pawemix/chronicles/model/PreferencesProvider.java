package pawemix.chronicles.model;

import java.util.List;
import java.util.regex.Pattern;

public interface PreferencesProvider {

    boolean assistantOnStartup();

    String[] getColorKeys();

    Pattern preferredRegex();

    List<Boolean> preferredAscendings();

    List<String> preferredColumnOrder();

    List<String> preferredVisibleCategories();
}
