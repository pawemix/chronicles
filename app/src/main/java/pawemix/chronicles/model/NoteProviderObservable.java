package pawemix.chronicles.model;

import java.util.List;

import pawemix.chronicles.framework.note.FirmNote;

public interface NoteProviderObservable {
    void addObserver(NoteObserver observer);

    void removeObserver(NoteObserver observer);

    void notifyNoteUpdated(FirmNote old, FirmNote updated);

    void notifyNotesUpdated(List<FirmNote> olds, List<FirmNote> updateds);

    void notifyNoteCreated(FirmNote created);

    void notifyNoteDeleted(FirmNote deleted);

    void notifyNotesDeleted(List<FirmNote> deleteds);
}
