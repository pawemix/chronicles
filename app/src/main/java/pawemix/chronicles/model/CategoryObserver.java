package pawemix.chronicles.model;

import android.os.Bundle;

public interface CategoryObserver {
    void onCategoryUpdated(String oldName, Bundle categoryBundle);
    void onCategoryCreated(Bundle categoryBundle);
    void onCategoryDeleted(Bundle categoryBundle);

    // groups of categories?
}
