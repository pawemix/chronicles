package pawemix.chronicles.framework;

import android.content.ContentValues;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

public class SimpleBundleable implements Bundleable {

    protected Bundle bundle;

    public SimpleBundleable() {
        this.bundle = new Bundle();
    }

    public SimpleBundleable(Bundle bundle) {

        this();
        this.bundle.putAll(bundle);
    }

    public static String now() {
        return new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    public static boolean bundlesEqual(Bundle first, Bundle second) {

        if(first.keySet().size() != second.keySet().size()) return false;

        if(!first.keySet().containsAll(second.keySet()) || !second.keySet().containsAll(first.keySet())) return false;

        for(String key : first.keySet())
            if(!Objects.equals(first.get(key), second.get(key))) return false;

        return true;
    }

    public static ContentValues buildContentValues(Bundle bundle, Collection<String> exceptions) {

        Set<String> keySet = bundle.keySet();
        for(String exception : exceptions) keySet.remove(exception);

        ContentValues output = new ContentValues(keySet.size());
        for(String key : keySet)
            if(bundle.get(key) != null)
                output.put(key, String.valueOf(bundle.get(key)));

        return output;
    }

    public static Bundle buildBundle(ContentValues values, Collection<String> exceptions) {

        Set<String> keySet = values.keySet();
        for(String exception : exceptions) keySet.remove(exception);

        Bundle output = new Bundle(keySet.size());

        for(String key : keySet)
            if(values.get(key) != null)
                output.putString(key, values.getAsString(key));

        return output;
    }

    @Override
    public String getString(String key) {
        return bundle.get(key) != null? String.valueOf(bundle.get(key)) : null;
    }

    @Override
    public void setString(String key, String value) {
        bundle.putString(key, value);
    }

    @Override
    public Long getLong(String key) {
        return bundle.get(key) != null? Long.parseLong(String.valueOf(bundle.get(key))) : null;
    }

    @Override
    public void setLong(String key, long value) {
        bundle.putLong(key, value);
    }

    @Override
    public Integer getInt(String key) {
        return bundle.get(key) != null? Integer.parseInt(String.valueOf(bundle.get(key))) : null;
    }

    @Override
    public void setInt(String key, int value) {
        bundle.putInt(key, value);
    }

    @Override
    public Bundle toBundle() {
        return new Bundle(bundle);
    }

    @Override
    public ContentValues toContentValues() {
        return buildContentValues(bundle, Collections.emptyList());
    }

    @Override
    public void updateBundle(Bundle update) {
        this.bundle.putAll(update);
    }

    @Override
    public void updateBundle(ContentValues values) {
        for(String key : values.keySet())
            if(values.get(key) != null)
                this.bundle.putString(key, values.getAsString(key));
    }

    @Override
    public boolean contains(String key) {
        return bundle.containsKey(key) && bundle.get(key) != null;
    }

    @Override
    public void setBundle(Bundle update) {
        bundle.clear();
        bundle.putAll(update);
    }

    @Override
    public boolean bundleEqualTo(Bundle bundle) {
        return SimpleBundleable.bundlesEqual(this.bundle, bundle);
    }
}
