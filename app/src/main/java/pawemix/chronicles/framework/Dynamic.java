package pawemix.chronicles.framework;

public interface Dynamic {

    boolean save();

    boolean revert();

    boolean isModified();
}
