package pawemix.chronicles.framework;

import android.content.ContentValues;
import android.os.Bundle;

import pawemix.chronicles.framework.note.LooseNote;

public interface Bundleable {

    String getString(String key);

    void setString(String key, String value);

    Long getLong(String key);

    void setLong(String key, long value);

    void setInt(String key, int value);

    Bundle toBundle();

    void updateBundle(Bundle update);

    void updateBundle(ContentValues values);

    boolean contains(String key);

    void setBundle(Bundle update);

    Integer getInt(String key);

    ContentValues toContentValues();

    boolean bundleEqualTo(Bundle bundle);
}
