package pawemix.chronicles.framework.category;

public class ChronicleCategory implements Category {
    private String name;
    private String colorKey;

    public ChronicleCategory(String name, String colorKey) {
        this.name = name;
        this.colorKey = colorKey;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String newName) {
        this.name = newName;
    }

    @Override
    public String getColorKey() {
        return colorKey;
    }

    @Override
    public void setColorKey(String colorKey) {
        this.colorKey = colorKey;
    }
}
