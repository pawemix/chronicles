package pawemix.chronicles.framework.category;

public interface Category {
    String getName();
    void setName(String newName);
    String getColorKey();
    void setColorKey(String colorKey);
}
