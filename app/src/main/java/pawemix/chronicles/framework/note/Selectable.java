package pawemix.chronicles.framework.note;

public interface Selectable {

    boolean isSelected();

    void setSelected(boolean selected);
}
