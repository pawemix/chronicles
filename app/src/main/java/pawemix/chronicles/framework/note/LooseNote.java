package pawemix.chronicles.framework.note;

import android.content.ContentValues;
import android.os.Bundle;

import java.util.Collections;

import pawemix.chronicles.framework.SimpleBundleable;
import pawemix.chronicles.model.DB;

public class LooseNote extends SimpleBundleable {

    public LooseNote(Bundle bundle) {
        super(bundle);
    }

    public String getTitle() {
        return getString(DB.NOTES_TITLE);
    }

    public void setTitle(String title) {
        setString(DB.NOTES_TITLE, title);
        setString(DB.NOTES_MODIFIED, now());
    }

    public String getContent() {
        return getString(DB.NOTES_CONTENT);
    }

    public void setContent(String content) {
        setString(DB.NOTES_CONTENT, content);
        setString(DB.NOTES_MODIFIED, now());
    }

    public String getCreated() {
        return getString(DB.NOTES_CREATED);
    }

    public String getModified() {
        return getString(DB.NOTES_MODIFIED);
    }

    public String getStart() {
        return getString(DB.NOTES_START);
    }

    public void setStart(String start) {
        setString(DB.NOTES_START, start);
        setString(DB.NOTES_MODIFIED, now());
    }

    public String getEnd() {
        return getString(DB.NOTES_END);
    }

    public void setEnd(String end) {
        setString(DB.NOTES_END, end);
        setString(DB.NOTES_MODIFIED, now());
    }

    public String getCategory() {
        return getString(DB.NOTES_CATEGORY);
    }

    public void setCategory(String category) {
        setString(DB.NOTES_CATEGORY, category);
        setString(DB.NOTES_MODIFIED, now());
    }

    public String getSection() {
        return getString(DB.NOTES_SECTION);
    }

    @Override
    public ContentValues toContentValues() {
        return SimpleBundleable.buildContentValues(bundle, Collections.singleton(DB.NOTES_ID));
    }

    public static String dateRange(String start, String end) {
        return start != null? (end != null? start + " - " + end : start) : "";
    }

    public static String logDates(String created, String modified) {
        return created != null? (modified != null? created + " <= " + modified : created) : "";
    }
}
