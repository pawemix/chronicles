package pawemix.chronicles.framework.note;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import pawemix.chronicles.R;
import pawemix.chronicles.model.DB;

public class FirmNote extends LooseNote implements Selectable {

    private boolean isSelected = false;

    public FirmNote(long id, Bundle bundle) {
        super(bundle);
        setLong(DB.NOTES_ID, id);
    }

    public long getID() {
        return getLong(DB.NOTES_ID);
    }

    // ======== ############### ======== //
    // ======== Metody selekcji ======== //
    // ======== ############### ======== //

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }
}
