package pawemix.chronicles.framework.filter;

public interface Filter<Type> {

    String clause();

    String[] args();

    MyPredicate<Type> predicate();
}
