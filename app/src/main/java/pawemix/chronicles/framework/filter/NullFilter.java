package pawemix.chronicles.framework.filter;

public class NullFilter<Type> implements Filter<Type> {

    @Override
    public String clause() {
        return null;
    }

    @Override
    public String[] args() {
        return null;
    }

    @Override
    public MyPredicate<Type> predicate() {
        return thing -> true;
    }
}
