package pawemix.chronicles.framework.filter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pawemix.chronicles.framework.Bundleable;

public class InFilter<Type extends Bundleable> implements Filter<Type> {

    private Filter<Type> decorated;
    private List<String> values;
    private String key;

    public InFilter(Filter<Type> decorated, List<String> values, String key) {

        this.decorated = decorated;
        this.values = values;
        this.key = key;
    }

    @Override
    public String clause() {

        if(values.size() < 1) return decorated.clause();

        String clause = "";
        String decoClause = decorated.clause();
        if(decoClause != null) clause += decoClause + " AND ";
        clause += "(" + key + " IN " +
                values.toString()
                        .replace('[', '(')
                        .replace(']', ')')
                        .replaceAll("\\w+", "?");
        clause += ")";

        return clause;
    }

    @Override
    public String[] args() {

        if(values.size() < 1) return decorated.args();

        String[] args;
        String[] decoArgs = decorated.args();
        if(decoArgs != null)
            args = Arrays.copyOf(decoArgs, decoArgs.length + values.size());
        else args = new String[values.size()];

        int index = 1;
        while(values.size() - index >= 0) {
            args[args.length - index] = values.get(values.size() - index);
            index++;
        }

        return args;
    }

    @Override
    public MyPredicate<Type> predicate() {
        return note -> {
            boolean contains = false;
            for(String category : values)
                if(note.getString(key).equalsIgnoreCase(category)) {
                    contains = true;
                    break;
                }
            return decorated.predicate().test(note) && contains;
        };
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
