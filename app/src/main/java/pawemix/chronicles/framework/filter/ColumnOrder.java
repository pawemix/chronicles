package pawemix.chronicles.framework.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import pawemix.chronicles.framework.Bundleable;

public class ColumnOrder<Type extends Bundleable> implements Order<Type> {
    private final List<String> columns;
    private final List<Boolean> ascendings;

    public ColumnOrder(List<String> columns, boolean ascending) {
        this.columns = columns;

        List<Boolean> ascendings = new ArrayList<>(columns.size());
        for(int i = 0; i < columns.size(); i++) ascendings.add(ascending);

        this.ascendings = ascendings;
    }

    public ColumnOrder(List<String> columns, List<Boolean> ascendings) {
        this.columns = columns;
        this.ascendings = ascendings;
    }

    @Override
    public Comparator<Type> comparator() {

        return (first, second) -> {

            for(int i = 0; i < columns.size(); i++) {

                String column = columns.get(i);
                boolean ascending = ascendings.get(i);

                String firstValue = first.getString(column);
                String secondValue = second.getString(column);
                if(firstValue == null) firstValue = "";
                if(secondValue == null) secondValue = "";

                if(!Objects.equals(firstValue, secondValue))
                    return ascending?
                            Objects.compare(firstValue, secondValue, String::compareTo):
                            Objects.compare(secondValue, firstValue, String::compareTo);
            }

            return 0;
        };
    }

    @Override
    public String clause() {

        if(Math.min(columns.size(), ascendings.size()) < 1) return null;

        StringBuilder clauseBuilder = new StringBuilder();

        if(Math.min(columns.size(), ascendings.size()) > 0)
            clauseBuilder.append(columns.get(0)).append(ascendings.get(0)? " ASC" : " DESC");

        for(int i = 1; i < Math.min(columns.size(), ascendings.size()); i++)
            clauseBuilder.append(", ").append(columns.get(i)).append(ascendings.get(i)? " ASC" : " DESC");

        return clauseBuilder.toString();
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<Boolean> getAscendings() {
        return ascendings;
    }
}
