package pawemix.chronicles.framework.filter;

import java.util.Arrays;
import java.util.regex.Pattern;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.model.DB;

public class RegexFilter<Type extends Bundleable> implements Filter<Type> {

    private Filter<Type> decorated;
    private Pattern pattern;
    private final String firstKey;
    private final String secondKey;

    public RegexFilter(Filter<Type> decorated, Pattern pattern, String firstKey, String secondKey) {

        this.decorated = decorated;
        this.pattern = pattern;
        this.firstKey = firstKey;
        this.secondKey = secondKey;
    }

    @Override
    public String clause() {

        String clause = "";
        String decoClause = decorated.clause();
        if(decoClause != null) clause += decoClause + " AND ";
        clause += "(" + firstKey + " REGEXP ? OR " + secondKey + " REGEXP ?)";

        return clause;
    }

    @Override
    public String[] args() {

        String[] args;
        String[] decoArgs = decorated.args();
        if(decoArgs != null)
            args = Arrays.copyOf(decoArgs, decoArgs.length + 2);
        else args = new String[2];
        args[args.length - 2] = args[args.length - 1] = this.pattern.pattern();

        return args;
    }

    @Override
    public MyPredicate<Type> predicate() {

        return note -> decorated.predicate().test(note)
                && (note.getString(firstKey).matches(pattern.pattern())
                || note.getString(secondKey).matches(pattern.pattern()));
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }
}
