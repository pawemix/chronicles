package pawemix.chronicles.framework.filter;

import java.util.Comparator;

public interface Order<Type> {

    Comparator<Type> comparator();

    String clause();
}
