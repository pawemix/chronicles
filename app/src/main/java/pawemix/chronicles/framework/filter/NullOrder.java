package pawemix.chronicles.framework.filter;

import java.util.Comparator;

import pawemix.chronicles.framework.Bundleable;

public class NullOrder<Type extends Bundleable> implements Order<Type> {

    @Override
    public String clause() {
        return null;
    }

    @Override
    public Comparator<Type> comparator() {
        return (first, second) -> Integer.compare(first.hashCode(), second.hashCode());
    }
}
