package pawemix.chronicles.framework.filter;

public interface MyPredicate<T> {
    boolean test(T t);
}
