package pawemix.chronicles.framework.filter;

import java.util.Arrays;

import pawemix.chronicles.framework.Bundleable;
import pawemix.chronicles.model.DB;

public class EqualsFilter<Type extends Bundleable> implements Filter<Type> {

    private Filter<Type> decorated;
    private String value;
    private String key;

    public EqualsFilter(Filter<Type> decorated, String value, String key) {

        this.decorated = decorated;
        this.value = value;
        this.key = key;
    }

    @Override
    public String clause() {

        String clause = "";
        String decoClause = decorated.clause();
        if(decoClause != null) clause += decoClause + " AND ";
        clause += "(" + key + " LIKE ?)";

        return clause;
    }

    @Override
    public String[] args() {

        String[] args;
        String[] decoArgs = decorated.args();
        if(decoArgs != null)
            args = Arrays.copyOf(decoArgs, decoArgs.length + 1);
        else args = new String[1];
        args[args.length - 1] = value;

        return args;
    }

    @Override
    public MyPredicate<Type> predicate() {
        return note -> decorated.predicate().test(note) && note.getString(key).equalsIgnoreCase(value);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
