package pawemix.chronicles.framework.checklist;

import android.os.Bundle;

import pawemix.chronicles.model.DB;

public class FirmChecklistItem extends LooseChecklistItem {

    public FirmChecklistItem(long id, long noteID, int index) {
        super(index);

        setLong(DB.CHECKLISTS_ID, id);
        setLong(DB.CHECKLISTS_NOTEID, noteID);
    }

    public FirmChecklistItem(long id, long noteID, int index, Bundle bundle) {

        super(index, bundle);

        setLong(DB.CHECKLISTS_ID, id);
        setLong(DB.CHECKLISTS_NOTEID, noteID);
    }

    public long getID() {
        return getLong(DB.CHECKLISTS_ID);
    }

    public long getNoteID() {
        return getLong(DB.CHECKLISTS_NOTEID);
    }
}
