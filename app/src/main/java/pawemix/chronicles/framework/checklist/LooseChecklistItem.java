package pawemix.chronicles.framework.checklist;

import android.content.ContentValues;
import android.os.Bundle;

import java.util.Collections;

import pawemix.chronicles.framework.SimpleBundleable;
import pawemix.chronicles.model.DB;

public class LooseChecklistItem extends SimpleBundleable {

    public LooseChecklistItem(int index) {

        bundle.putInt(DB.CHECKLISTS_INDEX, index);
        bundle.putInt(DB.CHECKLISTS_CHECKED, 0);
    }

    public LooseChecklistItem(int index, Bundle bundle) {

        updateBundle(bundle);
        bundle.putInt(DB.CHECKLISTS_INDEX, index);
        bundle.putBoolean(DB.CHECKLISTS_CHECKED, false);
    }

    public String getContent() {
        return getString(DB.CHECKLISTS_CONTENT);
    }

    public void setContent(String content) {
        setString(DB.CHECKLISTS_CONTENT, content);
    }

    public boolean isChecked() {
        return getInt(DB.CHECKLISTS_CHECKED) > 0;
    }

    public void setChecked(boolean checked) {
        setInt(DB.CHECKLISTS_CHECKED, checked? 1 : 0);
    }

    public void switchChecked() {
        setChecked(!isChecked());
    }

    public int getIndex() {
        return getInt(DB.CHECKLISTS_INDEX);
    }

    public void setIndex(int index) {
        setInt(DB.CHECKLISTS_INDEX, index);
    }

    @Override
    public ContentValues toContentValues() {
        return SimpleBundleable.buildContentValues(bundle, Collections.singleton(DB.CHECKLISTS_ID));
    }
}
