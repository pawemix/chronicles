package pawemix.chronicles.framework.checklist;

import pawemix.chronicles.framework.Dynamic;

public class DynamicFirmChecklistItem extends FirmChecklistItem implements Dynamic {

    private LooseChecklistItem backup;

    public DynamicFirmChecklistItem(FirmChecklistItem item) {
        super(item.getID(), item.getNoteID(), item.getIndex(), item.toBundle());
        backup = new FirmChecklistItem(item.getID(), item.getNoteID(), item.getIndex(), item.toBundle());
    }

    @Override
    public boolean save() {

        if(isModified()) {

            backup.updateBundle(toBundle());
            return true;
        }
        return false;
    }

    @Override
    public boolean revert() {

        if(isModified()) {

            setBundle(backup.toBundle());
            return true;
        }
        return false;
    }

    @Override
    public boolean isModified() {
        return !bundleEqualTo(backup.toBundle());
    }
}
