package pawemix.chronicles.view.categories;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.model.SimplePreferencesProvider;
import pawemix.chronicles.model.PreferencesProvider;

public class CategoryColorDialog extends DialogFragment {

    private Bundle args;

    @Override
    public void setArguments(Bundle args) {
        this.args = new Bundle(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String categoryName = args.getString(getString(R.string.dbkeys_notes_category));

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        CategoriesController controller = ArrayListCategoriesController.getInstance(
                DatabaseProvider.getInstance(getContext()));
        PreferencesProvider provider = SimplePreferencesProvider.getInstance(getContext());

        String[] availableColors = provider.getColorKeys();
        DialogInterface.OnClickListener apply = (dialogInterface, i) -> {
            String colorKey = availableColors[i];
            controller.setCategoryColorKey(categoryName, colorKey);
        };
        builder.setItems(availableColors, apply);
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dismiss());
        return builder.create();
    }
}
