package pawemix.chronicles.view.categories;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.model.SimplePreferencesProvider;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.PreferencesProvider;

public class CategoryCreateDialog extends DialogFragment implements TextWatcher {
    private CategoriesController controller;
    private EditText editText;
    private PreferencesProvider preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = ArrayListCategoriesController.getInstance(DatabaseProvider.getInstance(getContext()));
        preferences = SimplePreferencesProvider.getInstance(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller = null;
        preferences = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        editText = (EditText) View.inflate(getContext(),R.layout.dialog_createcat,null);
        editText.addTextChangedListener(this);
        builder.setView(editText);
        builder.setNegativeButton("Cancel",(dialogInterface, i) -> dismiss());
        builder.setPositiveButton("Create",(dialogInterface, i) -> {
            if(controller.categoryNameIsFree(editText.getText().toString())) {

                controller.addCategory(editText.getText().toString(), DB.DEFAULTS_COLORKEY);
            }
            else Toast.makeText(getContext(),"Category name is occupied.",Toast.LENGTH_SHORT).show();
        });
        return builder.create();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void afterTextChanged(Editable editable) {
        int colorID;
        if(controller.categoryNameIsFree(editable.toString())) colorID = R.color.greenDark;
        else colorID = R.color.redDark;
        editText.setTextColor(getContext().getColor(colorID));
    }
}
