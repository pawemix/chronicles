package pawemix.chronicles.view.categories;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.model.DatabaseProvider;

public class CategoryEditDialog extends DialogFragment implements DialogInterface.OnClickListener{
    private static final int RENAME = 0;
    private static final int COLOR = 1;
    private static final int DELETE = 2;

    private CategoriesController controller;
    private Bundle args;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = ArrayListCategoriesController.getInstance(DatabaseProvider.getInstance(getContext()));
    }

    @Override
    public void setArguments(Bundle args) {
        this.args = new Bundle(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setItems(new String[]{"Rename", "Change color", "Delete"},this);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        String categoryName = args.getString(getString(R.string.dbkeys_notes_category));

        Bundle arguments = new Bundle(1);
        arguments.putString(getContext().getString(R.string.dbkeys_notes_category), categoryName);
        switch(i) {
            case RENAME:
                DialogFragment renameDialog = new CategoryRenameDialog();
                renameDialog.setArguments(arguments);
                renameDialog.show(getFragmentManager(), getContext().getString(R.string.tags_categoryrenamedialog));
                break;
            case COLOR:
                DialogFragment colorDialog = new CategoryColorDialog();
                colorDialog.setArguments(arguments);
                colorDialog.show(getFragmentManager(), getContext().getString(R.string.tags_categorycolordialog));
                break;
            case DELETE:
                if(controller.categoryHasNotes(categoryName))
                    Toast.makeText(
                            getContext(),
                            "Category has notes. All will be assigned to " + getString(R.string.defaults_notes_category) + ".",
                            Toast.LENGTH_LONG
                    ).show();
                controller.deleteCategory(categoryName, getString(R.string.defaults_notes_category));
                Toast.makeText(getContext(),"Successfully deleted category.",Toast.LENGTH_SHORT).show();
                break;
        }
        dialogInterface.dismiss();
    }
}
