package pawemix.chronicles.view.categories;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.model.DatabaseProvider;

public class CategoryRenameDialog extends DialogFragment implements TextWatcher {
    private Bundle args;
    private CategoriesController controller;
    private EditText editText;
    private String categoryName;

    @Override
    public void setArguments(Bundle args) {
        this.args = new Bundle(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.categoryName = args.getString(getString(R.string.dbkeys_notes_category));

        controller = ArrayListCategoriesController.getInstance(DatabaseProvider.getInstance(getContext()));

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        editText = (EditText) LayoutInflater.from(getContext()).inflate(R.layout.dialog_renamecat, null);
        editText.setText(categoryName);
        editText.setTextColor(getContext().getColor(R.color.blueDark));
        editText.addTextChangedListener(this);
        builder.setView(editText);
        DialogInterface.OnClickListener apply = (dialogInterface, i) -> {

            if(controller.categoryNameIsFree(editText.getText().toString()))
                controller.setCategoryName(categoryName, editText.getText().toString());
            else if(!categoryName.equalsIgnoreCase(editText.getText().toString()))
                Toast.makeText(getContext(), "Category name is occupied.", Toast.LENGTH_SHORT).show();
        };
        builder.setPositiveButton("Rename", apply);
        builder.setNegativeButton("Cancel", ((dialogInterface, i) -> dismiss()));
        return builder.create();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        int colorID;
        if(controller.categoryNameIsFree(editable.toString())) colorID = R.color.greenDark;
        else if(editable.toString().equalsIgnoreCase(categoryName)) colorID = R.color.blueDark;
        else colorID = R.color.redDark;
        editText.setTextColor(getContext().getColor(colorID));
    }
}
