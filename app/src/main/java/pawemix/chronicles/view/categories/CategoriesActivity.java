package pawemix.chronicles.view.categories;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesObserver;
import pawemix.chronicles.controller.ObservableCategoriesController;
import pawemix.chronicles.framework.category.Category;
import pawemix.chronicles.model.DatabaseProvider;

public class CategoriesActivity extends AppCompatActivity implements CategoriesObserver {

    private ObservableCategoriesController categories;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        categories = ArrayListCategoriesController.getInstance(
                DatabaseProvider.getInstance(getApplicationContext()));
        categories.addObserver(this);

        setContentView(R.layout.activity_categories);

        FloatingActionButton flActButton = findViewById(R.id.categories_create);
        flActButton.setOnClickListener(view -> {
            DialogFragment fragment = new CategoryCreateDialog();
            fragment.show(getFragmentManager(), getString(R.string.tags_categoryCreateDialog));
        });

        recyclerView = findViewById(R.id.categories_recyclerview);
        RecyclerView.Adapter adapter = new CategoryRecyclerAdapter(categories, getFragmentManager());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        categories.removeObserver(this);
    }

    // ======== ############################## ======== //
    // ======== CATEGORIES CONTROLLER OBSERVER ======== //
    // ======== ############################## ======== //

    @Override
    public void onCategoriesReloaded() {

        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onCategoryUpdated(int position, Category category) {

        recyclerView.getAdapter().notifyItemChanged(position, category);
    }
}
