package pawemix.chronicles.view.categories;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.framework.category.Category;
import pawemix.chronicles.model.PreferencesProvider;
import pawemix.chronicles.model.SimplePreferencesProvider;

class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.CategoryViewHolder> {

    private final CategoriesController controller;
    private Context context;
    private FragmentManager fragmentManager;

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView nameView;

        CategoryViewHolder(View itemView) {

            super(itemView);
            nameView = itemView.findViewById(R.id.category_name);
        }
    }

    CategoryRecyclerAdapter(CategoriesController controller, FragmentManager fragmentManager) {

        this.controller = controller;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {

        Category category = controller.getCategoryAt(position);
        holder.nameView.setText(category.getName());
        holder.nameView.setTextColor(context.getColor(SimplePreferencesProvider.getDarkOf(category.getColorKey())));

        holder.nameView.setOnLongClickListener(view -> {

            DialogFragment fragment = new CategoryEditDialog();
            Bundle arguments = new Bundle(1);
            arguments.putString(context.getString(R.string.dbkeys_notes_category), category.getName());
            fragment.setArguments(arguments);
            fragment.show(fragmentManager, context.getString(R.string.tags_categoryeditdialog));
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return controller.getCategoriesAmount();
    }
}
