package pawemix.chronicles.view.categories;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.editor.EditorController;
import pawemix.chronicles.model.DatabaseProvider;

public class CategoryDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private EditorController editor;
    private CategoriesController categories;
    private int position = 0;
    public static final String POSITION_KEY = "position_key";

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        position = args.getInt(POSITION_KEY);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        DatabaseProvider provider = DatabaseProvider.getInstance(getContext());
        editor = ControllerStack.fetchCLEditor(position, this, provider, provider);
        categories = ArrayListCategoriesController.getInstance(provider);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Choose note category");
        CharSequence[] categoryNames = new CharSequence[categories.getCategoriesAmount()];
        builder.setItems(
                categories.getCategoryNames().toArray(categoryNames),
                (dialogInterface, i) -> editor.setCategory(categories.getCategoryAt(i).getName())
        );
        return builder.create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        editor = null;
        ControllerStack.abandonCLEditor(position, this);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        editor.setCategory(categories.getCategoryAt(i).getName());
    }
}
