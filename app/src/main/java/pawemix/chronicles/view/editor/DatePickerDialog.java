package pawemix.chronicles.view.editor;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.editor.EditorController;
import pawemix.chronicles.model.DatabaseProvider;

public abstract class DatePickerDialog extends DialogFragment {

    protected EditorController controller;

    public static final String KEY = "key";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        DatabaseProvider provider = DatabaseProvider.getInstance(getContext());

        controller = ControllerStack.fetchCLEditor(
                EditActivity.EDITOR_POSITION, this, provider, provider);

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(getContext(), null, year, month,
                dayOfMonth
        );
        DatePicker datePicker = dialog.getDatePicker();
        DialogInterface.OnClickListener next = (dialogInterface, i) -> createHourPickerDialog(
                String.format("%1$04d-%2$02d-%3$02d", datePicker.getYear(), datePicker.getMonth() + 1,
                        datePicker.getDayOfMonth()
                ));
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Next", next);
        DialogInterface.OnClickListener noDay = (dialogInterface, i) -> setEditedNoteDateTime(
                String.format("%1$04d-%2$02d", datePicker.getYear(), datePicker.getMonth()));
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No day", noDay);
        DialogInterface.OnClickListener onlyYear = (dialogInterface, i) -> setEditedNoteDateTime(
                String.format("%1$04d", datePicker.getYear()));
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Only year", onlyYear);
        return dialog;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        controller = null;
        ControllerStack.abandonCLEditor(EditActivity.EDITOR_POSITION, this);
    }

    protected abstract void createHourPickerDialog(String date);

    protected abstract void setEditedNoteDateTime(String date);

    protected abstract String getTitle();
}
