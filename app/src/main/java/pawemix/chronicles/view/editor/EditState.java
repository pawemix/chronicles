package pawemix.chronicles.view.editor;

public interface EditState {
    void onBackPressed();

    void onContentFocused();

    void onToolbarClicked();
}
