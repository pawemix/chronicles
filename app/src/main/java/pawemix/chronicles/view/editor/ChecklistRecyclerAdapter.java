package pawemix.chronicles.view.editor;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.editor.CLEditorController;
import pawemix.chronicles.controller.editor.CLEditorObserver;
import pawemix.chronicles.controller.editor.ObservableCLEditorController;

public class ChecklistRecyclerAdapter extends RecyclerView.Adapter<ChecklistViewHolder> {

    private final CLEditorController controller;
    private final ObservableCLEditorController observable;
    private final CLEditorObserver observer;

    public ChecklistRecyclerAdapter(CLEditorController controller, ObservableCLEditorController observable, CLEditorObserver observer) {
        this.controller = controller;
        this.observable = observable;
        this.observer = observer;
    }

    @Override
    public ChecklistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem_clitem, parent, false);

        ChecklistViewHolder holder = new ChecklistViewHolder(itemView);

        holder.setTextWatcher(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                observable.removeObserver(observer);
                controller.setItemContent(holder.getAdapterPosition(), editable.toString());
                observable.addObserver(observer);
            }
        });

        holder.getContentEditText().addTextChangedListener(holder.getTextWatcher());

        return holder;
    }

    @Override
    public void onBindViewHolder(ChecklistViewHolder holder, int position) {

        holder.getCheckBox().setOnCheckedChangeListener(null);

        holder.getContentEditText().removeTextChangedListener(holder.getTextWatcher());
        holder.getContentEditText().setText(controller.getItemContentAt(position));
        holder.getContentEditText().addTextChangedListener(holder.getTextWatcher());

        if(controller.isCheckedAt(position)) {
            holder.getContentEditText().setPaintFlags(
                    holder.getContentEditText().getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.getCheckBox().setChecked(true);
        }
        else {
            holder.getContentEditText().setPaintFlags(
                    holder.getContentEditText().getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            holder.getCheckBox().setChecked(false);
        }

        holder.getCheckBox().setOnCheckedChangeListener(
                (compoundButton, b) -> controller.setCheckedAt(holder.getAdapterPosition(), b));
    }

    @Override
    public int getItemCount() {
        return controller.checklistSize();
    }

}
