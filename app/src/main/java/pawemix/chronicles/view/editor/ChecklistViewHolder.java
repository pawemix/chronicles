package pawemix.chronicles.view.editor;

import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import pawemix.chronicles.R;

class ChecklistViewHolder extends RecyclerView.ViewHolder {

    private TextWatcher textWatcher;

    public ChecklistViewHolder(View itemView) {
        super(itemView);
    }

    public EditText getContentEditText() {
        return itemView.findViewById(R.id.clitem_content);
    }

    public CheckBox getCheckBox() {
        return itemView.findViewById(R.id.clitem_check);
    }

    public TextWatcher getTextWatcher() {
        return textWatcher;
    }

    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }
}
