package pawemix.chronicles.view.editor;

public class StartHourPickerDialog extends HourPickerDialog {
    @Override
    protected void setEditedNoteDate(String date) {
        controller.setStart(date);
    }

    @Override
    protected String getTitle() {
        return "Set start date";
    }
}
