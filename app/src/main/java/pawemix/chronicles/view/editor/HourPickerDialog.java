package pawemix.chronicles.view.editor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TimePicker;

import java.util.Calendar;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.editor.EditorController;
import pawemix.chronicles.model.DatabaseProvider;

abstract class HourPickerDialog extends DialogFragment {

    protected EditorController controller;
    protected String date;
    private int hour;
    private int minute;
    protected static final String KEY = "key";

    @Override
    public void setArguments(Bundle args) {

        this.date = args.getString(args.getString(KEY));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        controller = ControllerStack.fetchCLEditor(
                EditActivity.EDITOR_POSITION, this, DatabaseProvider.getInstance(getContext()),
                DatabaseProvider.getInstance(getContext())
        );

        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        TimePicker timePicker = (TimePicker) LayoutInflater.from(getContext()).inflate(
                R.layout.dialog_timepicker, null);
        timePicker.setIs24HourView(true);
        timePicker.setHour(hour);
        timePicker.setMinute(minute);
        timePicker.setOnTimeChangedListener((timePicker1, i, i1) -> {
            hour = i;
            minute = i1;
        });
        builder.setTitle(getTitle());
        builder.setView(timePicker);
        builder.setPositiveButton(
                "Apply",
                (dialogInterface, i) -> setEditedNoteDate(String.format(date + " %1$02d:%2$02d", hour, minute))
        );
        builder.setNegativeButton("No minute",
                (dialogInterface, i) -> setEditedNoteDate(String.format(date + " %1$02d", hour)));
        builder.setNeutralButton("No hour", (dialogInterface, i) -> setEditedNoteDate(date));
        return builder.create();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        controller = null;
        ControllerStack.abandonCLEditor(EditActivity.EDITOR_POSITION, this);
    }

    protected abstract void setEditedNoteDate(String date);

    protected abstract String getTitle();

}
