package pawemix.chronicles.view.editor;

import pawemix.chronicles.controller.editor.EditorController;

public class DefaultState implements EditState {
    private final EditorController controller;
    private final EditView view;

    public DefaultState(EditView view, EditorController controller) {
        this.view = view;
        this.controller = controller;
    }

    @Override
    public void onBackPressed() {
        if(controller.isModified()) view.warnUnsavedChanges();
        else view.leaveActivity();
    }

    @Override
    public void onContentFocused() {}

    @Override
    public void onToolbarClicked() {
        view.inflateRenameMenu();
        view.setState(new RenameState(view, controller));
    }
}
