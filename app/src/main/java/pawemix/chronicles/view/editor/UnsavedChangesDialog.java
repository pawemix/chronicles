package pawemix.chronicles.view.editor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.editor.EditorController;
import pawemix.chronicles.model.DatabaseProvider;

public class UnsavedChangesDialog extends DialogFragment {

    private EditorController controller = ControllerStack.fetchCLEditor(
            0, this, DatabaseProvider.getInstance(getContext()),
            DatabaseProvider.getInstance(getContext())
    );

    @Override
    public void onDestroy() {
        super.onDestroy();
        ControllerStack.abandonCLEditor(0, this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Warning");
        builder.setMessage("You have unsaved changes!");
        builder.setPositiveButton("Save", (dialogInterface, i) -> {
            controller.save();
            getActivity().finish();
        });
        builder.setNegativeButton("Discard", ((dialogInterface, i) -> {
            controller.revert();
            getActivity().finish();
        }));
        builder.setNeutralButton("Cancel", ((dialogInterface, i) -> dismiss()));
        return builder.create();
    }
}
