package pawemix.chronicles.view.editor;

import pawemix.chronicles.controller.editor.EditorController;

public final class RenameState implements EditState {

    private final EditView view;
    private final EditorController controller;

    public RenameState(EditView view, EditorController controller) {
        this.view = view;
        this.controller = controller;
    }

    @Override
    public void onBackPressed() {
        view.setState(new DefaultState(view, controller));
        view.inflateDefaultMenu();
    }

    @Override
    public void onContentFocused() {
        view.setState(new DefaultState(view, controller));
        view.inflateDefaultMenu();
    }

    @Override
    public void onToolbarClicked() {}
}
