package pawemix.chronicles.view.editor;

/**
 * Interfejs prezentujący modelowi funkcjonalności EditActivity, które model może wywołać.
 */
public interface EditView {

    void setContent(String content);

    void setTitle(String title);

    void onBackPressed();

    void leaveActivity();

    void inflateDefaultMenu();

    void inflateRenameMenu();

    void repaintToolbar();

    void repaintCategoryButton();

    void warnUnsavedChanges();

    void setState(EditState state);
}
