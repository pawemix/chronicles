package pawemix.chronicles.view.editor;

public class EndHourPickerDialog extends HourPickerDialog {
    @Override
    protected void setEditedNoteDate(String date) {
        controller.setEnd(date);
    }

    @Override
    protected String getTitle() {
        return "Set end date";
    }
}
