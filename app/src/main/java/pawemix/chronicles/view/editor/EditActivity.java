package pawemix.chronicles.view.editor;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.editor.CLEditorController;
import pawemix.chronicles.controller.editor.CLEditorObserver;
import pawemix.chronicles.controller.editor.EditorObserver;
import pawemix.chronicles.controller.editor.ObservableCLEditorController;
import pawemix.chronicles.controller.editor.ObservableEditorController;
import pawemix.chronicles.controller.editor.SimpleCLEditorController;
import pawemix.chronicles.framework.note.LooseNote;
import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.model.PreferencesProvider;
import pawemix.chronicles.model.SimplePreferencesProvider;
import pawemix.chronicles.view.assistant.NoteAssistantService;
import pawemix.chronicles.view.categories.CategoryDialog;

public class EditActivity extends AppCompatActivity implements EditView, EditorObserver, CLEditorObserver {

    private CLEditorController editor;
    private CategoriesController categories;
    private ObservableEditorController observable;
    private ObservableCLEditorController observableCL;

    private EditState state;

    private RecyclerView checklistRecycler;
    private EditText contentEditText;
    private Toolbar toolbar;
    private ConstraintLayout dates;
    private TextView startDate;
    private TextView endDate;
    private Menu menu;
    private TextWatcher contentTextWatcher;
    private TextWatcher titleTextWatcher;
    public static final int EDITOR_POSITION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);

        setContentView(R.layout.activity_edit);

        DatabaseProvider provider = DatabaseProvider.getInstance(getApplicationContext());

        SimpleCLEditorController targetController = ControllerStack.fetchCLEditor(
                0, this, provider, provider);

        editor = targetController;
        (observable = targetController).addObserver(this);
        (observableCL = targetController).addObserver(this);

        PreferencesProvider preferences = SimplePreferencesProvider.getInstance(getApplicationContext());
        categories = ArrayListCategoriesController.getInstance(DatabaseProvider.getInstance(getApplicationContext()));

        contentEditText = findViewById(R.id.edit_notecontent);
        dates = findViewById(R.id.edit_dates);
        startDate = findViewById(R.id.edit_startdate);
        endDate = findViewById(R.id.edit_enddate);
        toolbar = findViewById(R.id.edit_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View customView = getLayoutInflater().inflate(R.layout.toolbar_rename, toolbar, false);
        getSupportActionBar().setCustomView(customView, new Toolbar.LayoutParams(
                Toolbar.LayoutParams.MATCH_PARENT,
                Toolbar.LayoutParams.MATCH_PARENT
        ));

        checklistRecycler = findViewById(R.id.edit_checklist);

        checklistRecycler.setAdapter(new ChecklistRecyclerAdapter(editor, observableCL, this));
        checklistRecycler.setHasFixedSize(false);
        checklistRecycler.setLayoutManager(
                new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return editor.swapItems(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                editor.removeItemAt(viewHolder.getAdapterPosition());
            }
        };

        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(checklistRecycler);

        ConstraintLayout addCLItem = findViewById(R.id.edit_addclitem);
        addCLItem.setOnClickListener(view -> editor.addItem());

        if(preferences.assistantOnStartup()) {
            Intent assistant = new Intent();
            assistant.setClass(getApplicationContext(), NoteAssistantService.class);
            assistant.setAction(Intent.ACTION_MAIN);
            assistant.addCategory(Intent.CATEGORY_DEFAULT);
            startService(assistant);
        }

        if(getIntent() != null && getIntent().getAction() != null) {

            if(getIntent().getAction().equalsIgnoreCase(Intent.ACTION_EDIT)) {

                if(getIntent().getData() != null)
                    editor.loadNote(Long.decode(getIntent().getData().getLastPathSegment()));

                else
                    editor.initializeNote();
            }

            else if(getIntent().getAction().equalsIgnoreCase(Intent.ACTION_SEND) && getIntent().getExtras() != null)
                editor.setContent(editor.getContent() + "\n" + getIntent().getExtras().getString(Intent.EXTRA_TEXT));
        }

        state = new DefaultState(this, editor);

        toolbar.setOnClickListener(view -> state.onToolbarClicked());

        startDate.setText(editor.getStart() != null? editor.getStart() : "-");
        startDate.setOnClickListener(view -> {
            DialogFragment startDateTimeDialog = new StartDatePickerDialog();
            startDateTimeDialog.show(getFragmentManager(), "dialog_datetime_start");
        });

        endDate.setText(editor.getEnd() != null? editor.getEnd() : "-");
        endDate.setOnClickListener(view -> {
            DialogFragment endDateTimeDialog = new EndDatePickerDialog();
            endDateTimeDialog.show(getFragmentManager(), "dialog_datetime_end");
        });

        contentEditText.setOnFocusChangeListener((view, hasFocus) -> {
            if(hasFocus) state.onContentFocused();
        });
        getSupportActionBar().setTitle(editor.getTitle());
        toolbar.setNavigationOnClickListener(view -> state.onBackPressed());
        repaintToolbar();
        contentTextWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                editor.setContent(editable.toString());
            }
        };
        contentEditText.removeTextChangedListener(null);
        contentEditText.setText(editor.getContent());
        contentEditText.addTextChangedListener(contentTextWatcher);

        titleTextWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                editor.setTitle(editable.toString());
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        observable.removeObserver(this);
        observableCL.removeObserver(this);
        observable = null;
        observableCL = null;
        editor = null;

        ControllerStack.abandonCLEditor(EDITOR_POSITION, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_edit_revert:
                if(editor.revert())
                    Toast.makeText(getBaseContext(), "Successfully reverted note.", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getBaseContext(), "Nothing to revert.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_edit_save:
                if(editor.save())
                    Toast.makeText(getBaseContext(), "Successfully saved note.", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getBaseContext(), "Nothing to save.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_edit_archive:
                editor.archiveNote();
                Toast.makeText(getBaseContext(), "Successfully archived note.", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case R.id.menu_edit_trash:
                editor.trashNote();
                Toast.makeText(getBaseContext(), "Successfully trashed note.", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case R.id.menu_edit_rename:
                state = new RenameState(this, editor);
                inflateRenameMenu();
                break;
        }
        return true;
    }

    @Override
    public void setContent(String content) {
        this.contentEditText.setText(content);
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        state = new DefaultState(this, editor);
        inflateDefaultMenu();
        return true;
    }

    public void leaveActivity() {
        finish();
    }

    @Override
    public void inflateDefaultMenu() {
        menu.clear();
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        contentEditText.clearFocus();
    }

    @Override
    public void inflateRenameMenu() {
        menu.clear();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        Button button = findViewById(R.id.rename_category);
        button.setOnClickListener(view -> {
            DialogFragment categoryFragment = new CategoryDialog();
            Bundle args = new Bundle(1);
            args.putInt(CategoryDialog.POSITION_KEY, EDITOR_POSITION);
            categoryFragment.setArguments(args);
            categoryFragment.show(getFragmentManager(), "category");
        });
        repaintCategoryButton();
        EditText renameEditText = findViewById(R.id.rename_edittext);
        renameEditText.setText(editor.getTitle());
        renameEditText.addTextChangedListener(titleTextWatcher);
        renameEditText.requestFocus();
    }

    @Override
    public void repaintToolbar() {

        toolbar.setBackgroundColor(
                getColor(SimplePreferencesProvider.getLightOf(categories.getKeyOf(editor.getCategory()))));

        dates.setBackgroundColor(
                getColor(SimplePreferencesProvider.getDarkOf(categories.getKeyOf(editor.getCategory()))));

        getWindow().setStatusBarColor(
                getColor(SimplePreferencesProvider.getDarkOf(categories.getKeyOf(editor.getCategory()))));
    }

    @Override
    public void repaintCategoryButton() {

        Button button = findViewById(R.id.rename_category);

        if(button != null) {
            button.getBackground().setColorFilter(
                    getColor(SimplePreferencesProvider.getDarkOf(categories.getKeyOf(editor.getCategory()))),
                    PorterDuff.Mode.CLEAR
            );
        }
    }

    @Override
    public void warnUnsavedChanges() {
        DialogFragment dialog = new UnsavedChangesDialog();
        dialog.show(getFragmentManager(), "unsavedChangesDialog");
    }

    @Override
    public void setState(EditState state) {
        this.state = state;
    }

    @Override
    public void onBackPressed() {
        state.onBackPressed();
    }

// ======== ######## ======== //
// ======== OBSERVER ======== //
// ======== ######## ======== //

    @Override
    public void onNoteUpdated(LooseNote note) {

        repaintCategoryButton();
        repaintToolbar();

        startDate.setText(editor.getStart() != null? editor.getStart() : "-");
        endDate.setText(editor.getEnd() != null? editor.getEnd() : "-");

        getSupportActionBar().setTitle(note.getTitle());
        EditText renameEditText = findViewById(R.id.rename_edittext);
        if(renameEditText != null && !renameEditText.getText().toString().equals(note.getTitle())) {

            renameEditText.removeTextChangedListener(titleTextWatcher);
            renameEditText.setText(note.getTitle());
            renameEditText.addTextChangedListener(titleTextWatcher);
        }

        if(!contentEditText.getText().toString().equals(note.getContent())) {

            contentEditText.removeTextChangedListener(contentTextWatcher);
            contentEditText.setText(note.getContent());
            contentEditText.addTextChangedListener(contentTextWatcher);
        }
    }

// ======== ########### ======== //
// ======== CL OBSERVER ======== //
// ======== ########### ======== //

    @Override
    public void onItemUpdated(int position) {
        checklistRecycler.getAdapter().notifyItemChanged(position);
    }

    @Override
    public void onItemRemoved(int position) {
        checklistRecycler.getAdapter().notifyItemRemoved(position);
    }

    @Override
    public void onItemInserted(int position) {
        checklistRecycler.getAdapter().notifyItemInserted(position);
    }

    @Override
    public void onItemsReloaded() {
        checklistRecycler.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onItemMoved(int from, int to) {
        checklistRecycler.getAdapter().notifyItemMoved(from, to);
    }
}


