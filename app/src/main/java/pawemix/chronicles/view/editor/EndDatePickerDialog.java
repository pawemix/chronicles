package pawemix.chronicles.view.editor;

import android.app.DialogFragment;
import android.os.Bundle;

import pawemix.chronicles.R;

public final class EndDatePickerDialog extends DatePickerDialog {

    @Override
    protected void createHourPickerDialog(String date) {
        DialogFragment dialog = new EndHourPickerDialog();
        Bundle arguments = new Bundle(2);
        arguments.putString(HourPickerDialog.KEY, getString(R.string.dbkeys_notes_dateend));
        arguments.putString(getString(R.string.dbkeys_notes_dateend), date);
        dialog.setArguments(arguments);
        dialog.show(getFragmentManager(),"EndHourPickerDialog");
    }

    @Override
    protected void setEditedNoteDateTime(String date) {
        controller.setEnd(date);
    }

    @Override
    protected String getTitle() {
        return "Set end date";
    }
}
