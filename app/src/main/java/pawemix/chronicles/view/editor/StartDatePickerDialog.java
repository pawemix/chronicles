package pawemix.chronicles.view.editor;

import android.app.DialogFragment;
import android.os.Bundle;

import pawemix.chronicles.R;

public final class StartDatePickerDialog extends DatePickerDialog {

    @Override
    protected void createHourPickerDialog(String date) {
        DialogFragment dialog = new StartHourPickerDialog();
        Bundle arguments = new Bundle(2);
        arguments.putString(DatePickerDialog.KEY, getString(R.string.dbkeys_notes_datestart));
        arguments.putString(getString(R.string.dbkeys_notes_datestart),date);
        dialog.setArguments(arguments);
        dialog.show(getFragmentManager(),"startHourPickerDialog");
    }

    @Override
    protected void setEditedNoteDateTime(String date) {
        controller.setStart(date);
    }

    @Override
    protected String getTitle() {
        return "Set start date";
    }
}
