package pawemix.chronicles.view.assistant;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.editor.EditorController;
import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.view.editor.EditActivity;

public class NoteManagerService extends Service {
    private EditorController controller;

    @Override
    public void onCreate() {
        super.onCreate();
        controller = ControllerStack.fetchCLEditor(
                EditActivity.EDITOR_POSITION,
                this,
                DatabaseProvider.getInstance(getApplicationContext()),
                DatabaseProvider.getInstance(getApplicationContext())
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller = null;
        ControllerStack.abandonCLEditor(EditActivity.EDITOR_POSITION, this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null && intent.getAction() != null) {

            if(intent.getAction().equalsIgnoreCase(getString(R.string.actions_updateeditednote)) && intent.getExtras() != null) {
                Bundle updateBundle = intent.getExtras();
                if(updateBundle.containsKey(getString(R.string.dbkeys_notes_content)))
                    controller.setContent(updateBundle.getString(getString(R.string.dbkeys_notes_content)));
                if(updateBundle.containsKey(getString(R.string.dbkeys_notes_title)))
                    controller.setTitle(updateBundle.getString(getString(R.string.dbkeys_notes_title)));
                controller.save();
            }

            else if(intent.getAction().equalsIgnoreCase(getString(R.string.actions_putcurrenttime))) {

                final String now = new SimpleDateFormat("HH:mm",Locale.getDefault()).format(new Date());
                controller.setContent(controller.getContent() + "\n" + now);

            }

            else if(intent.getAction().equalsIgnoreCase(Intent.ACTION_SEND) && intent.getExtras() != null) {

                if(!intent.getExtras().containsKey(Intent.EXTRA_TEXT))
                    Toast.makeText(getBaseContext(),"Plain text not found. Other extras not handled (yet).",Toast.LENGTH_SHORT).show();

                else {
                    controller.setContent(controller.getContent() + "\n" + intent.getExtras().getString(Intent.EXTRA_TEXT));
                    controller.save();
                }
            }

            else if(intent.getAction().equalsIgnoreCase(getString(R.string.actions_initializenotetoedit))) {

                if(controller.isModified()) controller.save();

                controller.initializeNote();

            } else if(intent.getAction().equalsIgnoreCase(getString(R.string.actions_puttext))) {

                // TODO komunikat z tekstem do doklejenia/zastąpienia contentu
                Toast.makeText(getBaseContext(), "PutText not yet implemented! Sorry.", Toast.LENGTH_SHORT).show();

            } else if(intent.getAction().equalsIgnoreCase(getString(R.string.actions_readmedia))) {

                // TODO retriever powinien być wyciągany z systemu, zależnie od tego co(czy coś) jest odtwarzane
                Toast.makeText(getBaseContext(),"Read Media not yet implemented! Sorry..",Toast.LENGTH_SHORT).show();
//                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                String metadata = "\n~";
//                metadata += retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
//                metadata += " - ";
//                metadata += retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
//                mvc.setContent(mvc.getContent() + metadata);
//                retriever.release();
            }

        } else Toast.makeText(getBaseContext(), "Invalid intent.", Toast.LENGTH_SHORT).show();
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
