package pawemix.chronicles.view.assistant;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pawemix.chronicles.model.SimplePreferencesProvider;
import pawemix.chronicles.model.PreferencesProvider;

public class ChronicleReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        PreferencesProvider provider = SimplePreferencesProvider.getInstance(context);

        if(intent.getAction() != null &&
                intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED) &&
                provider.assistantOnStartup()
        ) {
            Intent noteAssistantService = new Intent();
            noteAssistantService.setAction(Intent.ACTION_MAIN);
            noteAssistantService.setClass(context, NoteAssistantService.class);
            context.startService(noteAssistantService);
        }
    }
}
