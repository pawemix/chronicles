package pawemix.chronicles.view.assistant;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListBrowserController;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.ControllerStackObserver;
import pawemix.chronicles.controller.editor.EditorController;
import pawemix.chronicles.controller.editor.EditorObserver;
import pawemix.chronicles.controller.editor.ObservableEditorController;
import pawemix.chronicles.controller.RawContextHolder;
import pawemix.chronicles.controller.editor.SimpleCLEditorController;
import pawemix.chronicles.controller.editor.SimpleEditorController;
import pawemix.chronicles.framework.note.LooseNote;
import pawemix.chronicles.view.browser.BrowseActivity;
import pawemix.chronicles.view.editor.EditActivity;

public class NoteAssistantService extends Service implements EditorObserver, ControllerStackObserver {

    private static final int NOTIFICATION_ID = 256;
    private static final int SECTION_DEFAULT = 0;
    private static final int SECTION_ARCHIVE = 1;
    private static final int SECTION_TRASHBIN = 2;
    private Notification notification;
    private EditorController controller;
    private ObservableEditorController observable;

    private RawContextHolder c;

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {

        super.onCreate();

        c = new RawContextHolder(getApplicationContext());

        ControllerStack.addObserver(this);
        SimpleEditorController targetController = ControllerStack.peekCLEditor(EditActivity.EDITOR_POSITION);

        if(targetController != null) {
            controller = targetController;
            observable = targetController;
            observable.addObserver(this);
        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        ControllerStack.removeObserver(this);

        if(observable != null) observable.removeObserver(this);

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.cancel(NOTIFICATION_ID);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        NotificationCompat.Builder builder;

        if(controller != null) {

            int section = SECTION_DEFAULT;
            if(controller.getSection().equalsIgnoreCase(c.SECTIONS_ARCHIVE)) section = SECTION_ARCHIVE;
            else if(controller.getSection().equalsIgnoreCase(c.SECTIONS_TRASHBIN)) section = SECTION_TRASHBIN;

            builder = setupEditorLayout(
                    controller.getTitle(), controller.getContent(), controller.isModified(), section
            );
        }
        else
            builder = setupInitLayout();

        notification = builder.build();

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(NOTIFICATION_ID, notification);

        return START_STICKY;
    }

    private NotificationCompat.Builder setupEditorLayout(String title, String content, boolean modified, int section) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getApplicationContext(),
                getString(R.string.tags_notificationchannel)
        );

        String processedTitle = "";

        if(section == SECTION_ARCHIVE) processedTitle += "[archive] ";

        else if(section == SECTION_TRASHBIN) processedTitle += "[trash] ";

        if(modified) processedTitle += "[unsaved] ";

        if(title != null) processedTitle += title;

        builder.setSmallIcon(R.drawable.ic_deer_second_white_24dp);
        builder.setColor(getColor(R.color.primary));
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setContentTitle(processedTitle);
        builder.setContentText(content);

        Intent toEditor = new Intent(
                Intent.ACTION_MAIN, null, getBaseContext(), EditActivity.class);
        toEditor.addCategory(Intent.CATEGORY_LAUNCHER);
        Intent initialize = new Intent(
                getString(R.string.actions_initializenotetoedit), null, getBaseContext(),
                NoteManagerService.class
        );
        Intent putNow = new Intent(
                getString(R.string.actions_putcurrenttime), null, getBaseContext(),
                NoteManagerService.class
        );
        Intent readMedia = new Intent(
                getString(R.string.actions_readmedia), null, getBaseContext(),
                NoteManagerService.class
        );

        PendingIntent pendingToEditor = PendingIntent.getActivity(
                getBaseContext(), 0, toEditor, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingCreateNew = PendingIntent.getService(
                getBaseContext(), 0, initialize, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingPutNow = PendingIntent.getService(
                getBaseContext(), 0, putNow, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendindReadMedia = PendingIntent.getService(
                getBaseContext(), 0, readMedia, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingToEditor);
        builder.addAction(R.drawable.ic_create_black_24dp, "Initialize new", pendingCreateNew);
        builder.addAction(R.drawable.ic_time_white_24dp, "Now", pendingPutNow);
        builder.addAction(R.drawable.ic_music_white_24dp, "Read media", pendindReadMedia);

        return builder;
    }

    private NotificationCompat.Builder setupInitLayout() {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getBaseContext(), getString(R.string.tags_notificationchannel));

        builder.setSmallIcon(R.drawable.ic_deer_second_white_24dp);
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setColor(getColor(R.color.primary));
        builder.setContentTitle("No note selected");
        builder.setContentText("Tap to go to the browser");

        Intent toBrowser = new Intent(
                Intent.ACTION_MAIN, null, getBaseContext(), BrowseActivity.class);
        toBrowser.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pendingToBrowser = PendingIntent.getActivity(
                getBaseContext(), 0, toBrowser, PendingIntent.FLAG_IMMUTABLE);
        Intent initNote = new Intent(
                getString(R.string.actions_initializenotetoedit), null, getBaseContext(),
                NoteManagerService.class
        );
        PendingIntent pendingInitNote = PendingIntent.getService(
                getBaseContext(), 0, initNote, PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(pendingToBrowser);
        builder.addAction(R.drawable.ic_create_black_24dp, "Create", pendingInitNote);

        return builder;
    }

    // ======== Editor Observer ======== //

    @Override
    public void onNoteUpdated(LooseNote note) {

        NotificationCompat.Builder builder;

        if(note != null) {

            int section = SECTION_DEFAULT;
            if(note.getSection().equalsIgnoreCase(c.SECTIONS_ARCHIVE)) section = SECTION_ARCHIVE;
            else if(note.getSection().equalsIgnoreCase(c.SECTIONS_TRASHBIN)) section = SECTION_TRASHBIN;

            builder = setupEditorLayout(note.getTitle(), note.getContent(), controller.isModified(), section);
        }

        else builder = setupInitLayout();

        notification = builder.build();
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(NOTIFICATION_ID, notification);
    }

    // ======== Stack Observer ======== //

    @Override
    public void onCLEditorCreated(int position, SimpleCLEditorController controller) {

        if(position == EditActivity.EDITOR_POSITION) {

            this.controller = controller;
            observable = controller;
            observable.addObserver(this);

            NotificationCompat.Builder builder;

            int section = SECTION_DEFAULT;
            if(controller.getSection().equalsIgnoreCase(c.SECTIONS_ARCHIVE)) section = SECTION_ARCHIVE;
            else if(controller.getSection().equalsIgnoreCase(c.SECTIONS_TRASHBIN)) section = SECTION_TRASHBIN;

            builder = setupEditorLayout(
                    controller.getTitle(), controller.getContent(), controller.isModified(), section
            );

            notification = builder.build();

            NotificationManagerCompat manager = NotificationManagerCompat.from(this);
            manager.notify(NOTIFICATION_ID, notification);
        }
    }

    @Override
    public void onCLEditorDied(int position) {

        if(position == EditActivity.EDITOR_POSITION) {

            controller = null;
            observable = null;

            NotificationCompat.Builder builder = setupInitLayout();
            notification = builder.build();
            NotificationManagerCompat manager = NotificationManagerCompat.from(this);
            manager.notify(NOTIFICATION_ID, notification);
        }
    }

    @Override
    public void onBrowserCreated(int position, ArrayListBrowserController controller) {}

    @Override
    public void onBrowserDied(int position) {}
}