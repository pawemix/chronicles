package pawemix.chronicles.view.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pawemix.chronicles.R;
import pawemix.chronicles.view.browser.SortDialogAdapter;

public abstract class MultiSelectPreference extends DialogPreference {

    // TODO ostatni etap tworzenia rozszerzenia DialogPreference - odpowiedź UI na rzeczy typu obrócenie ekranu

    protected List<String> strings;
    protected List<Boolean> booleans;

    public MultiSelectPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.dialog_sortnotes);

        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        setDialogIcon(null);
    }

    public static List<String> buildStrings(String sequence) {

        String[] table = sequence.split(",");
        List<String> output = new ArrayList<>(table.length / 2);

        for(int i = 0; i < table.length; i += 2) output.add(table[i].trim().replaceAll("\\n", ""));

        return output;
    }

    public static List<Boolean> buildBooleans(String sequence) {

        String[] table = sequence.split(",");
        List<Boolean> output = new ArrayList<>(table.length / 2);

        for(int i = 1; i < table.length; i += 2)
            output.add(Boolean.parseBoolean(table[i].replaceAll("\\W+", "")));

        return output;
    }

    public static List<String> buildTrueStrings(String sequence) {

        List<String> strings = buildStrings(sequence);
        List<Boolean> booleans = buildBooleans(sequence);

        Iterator<String> sIterator = strings.iterator();
        Iterator<Boolean> bIterator = booleans.iterator();

        while(sIterator.hasNext() && bIterator.hasNext()) {

            sIterator.next();
            boolean bool = bIterator.next();

            if(!bool) {

                sIterator.remove();
                bIterator.remove();
            }
        }

        return strings;
    }

    protected static String buildSequence(List<String> strings, List<Boolean> booleans) {

        int size = Math.min(strings.size(), booleans.size());

        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < size; i++) builder.append(strings.get(i)).append(',').append(booleans.get(i)).append(',');

        builder.deleteCharAt(builder.length() - 1);

        return builder.toString();
    }

    protected abstract String defaultSequence();

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {

        String sequence;
        if(restorePersistedValue) sequence = getPersistedString(defaultSequence());
        else sequence = (String) defaultValue;

        strings = buildStrings(sequence);
        booleans = buildBooleans(sequence);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if(positiveResult) {
            persistString(buildSequence(strings, booleans));
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onBindDialogView(View view) {

        super.onBindDialogView(view);

        RecyclerView recycler = view.findViewById(R.id.sortnotes_recycler);
        recycler.setAdapter(new SortDialogAdapter(strings, booleans));
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        onBindRecycler(recycler);
    }

    protected abstract void onBindRecycler(RecyclerView recycler);
}
