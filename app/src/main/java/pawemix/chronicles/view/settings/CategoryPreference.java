package pawemix.chronicles.view.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.model.DatabaseProvider;

public class CategoryPreference extends MultiSelectPreference {

    public CategoryPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected String defaultSequence() {
        return buildDefaultSequence(getContext());
    }

    @Override
    protected void onBindRecycler(RecyclerView recycler) {
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {

        String sequence = defaultSequence();

        strings = buildStrings(sequence);
        booleans = buildBooleans(sequence);

        if(restorePersistedValue) {

            String oldSequence = getPersistedString(defaultSequence());

            if(!oldSequence.equals(sequence)) {

                List<String> oldStrings = buildStrings(oldSequence);
                List<Boolean> oldBooleans = buildBooleans(oldSequence);
                int size = Math.min(oldStrings.size(), oldBooleans.size());

                for(int i = 0; i < size; i++)
                    if(strings.contains(oldStrings.get(i)))
                        booleans.set(i, oldBooleans.get(i));
            }
        }
    }

    public static String buildDefaultSequence(Context context) {

        CategoriesController categories = ArrayListCategoriesController.getInstance(
                DatabaseProvider.getInstance(context));

        List<String> strings = categories.getCategoryNames();
        List<Boolean> booleans = new ArrayList<>(Collections.nCopies(strings.size(), true));

        return MultiSelectPreference.buildSequence(strings, booleans);
    }
}
