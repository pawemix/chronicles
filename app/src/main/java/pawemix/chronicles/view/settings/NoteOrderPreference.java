package pawemix.chronicles.view.settings;

import android.content.Context;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pawemix.chronicles.model.DB;

public class NoteOrderPreference extends MultiSelectOrderedPreference {

    public NoteOrderPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected String defaultSequence() {
        return defaultOrderSequence();
    }

    public static String defaultOrderSequence() {
        List<String> columns = new ArrayList<>(
                Arrays.asList(
                        DB.NOTES_START, DB.NOTES_END, DB.NOTES_CREATED, DB.NOTES_MODIFIED, DB.NOTES_TITLE,
                        DB.NOTES_CONTENT, DB.NOTES_CATEGORY, DB.NOTES_SECTION, DB.NOTES_ID
                ));
        List<Boolean> ascendings = new ArrayList<>(Collections.nCopies(columns.size(), true));

        return MultiSelectPreference.buildSequence(columns, ascendings);
    }
}
