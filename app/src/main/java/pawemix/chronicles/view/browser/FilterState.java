package pawemix.chronicles.view.browser;

import pawemix.chronicles.controller.BrowserController;

public class FilterState implements BrowseState {
    private final BrowserController controller;
    private final BrowseView view;
    
    FilterState(BrowserController controller, BrowseView view) {
        this.controller = controller;
        this.view = view;
    }

    @Override
    public void tapNote(int position, Long id) {
        view.setState(new ChooseState(controller, view));
        view.inflateChooseMenu();
        view.launchEditor(id);
    }

    @Override
    public void longClickNote(int position, Long id) {
        view.setState(new SelectState(controller, view));
        view.inflateSelectMenu();
        controller.selectNoteAt(position);
    }

    @Override
    public void onBackPressed() {
        view.setState(new ChooseState(controller, view));
        view.inflateChooseMenu();
    }

    @Override
    public void onDrawerOpened() {
        view.setState(new DrawerState(view, this));
    }

    @Override
    public void onDrawerClosed() {}

    @Override
    public void toSelectState() {
        view.setState(new SelectState(controller, view));
        view.inflateSelectMenu();
    }

    @Override
    public void toChooseState() {
        view.setState(new ChooseState(controller, view));
        view.inflateChooseMenu();
    }

    @Override
    public void toFilterState() {}

    @Override
    public void onHomeTapped() {
        onBackPressed();
    }
}
