package pawemix.chronicles.view.browser;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public interface SwiperState {

    void onSwiped(View rootView, NoteViewHolder holder, int direction);

    void onChildDraw(Canvas c, RecyclerView rv, RecyclerView.ViewHolder vh, float dX, float dY, boolean active);
}
