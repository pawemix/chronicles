package pawemix.chronicles.view.browser;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.BrowserController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.framework.note.FirmNote;
import pawemix.chronicles.framework.note.LooseNote;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.PreferencesProvider;
import pawemix.chronicles.model.SimplePreferencesProvider;

public class NoteRecyclerAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    private NoteRecyclerDelegate delegate;
    private BrowserController browser;
    private CategoriesController categories;
    private PreferencesProvider preferences;

    NoteRecyclerAdapter(BrowserController browser, CategoriesController categories, NoteRecyclerDelegate delegate) {

        this.delegate = delegate;
        this.browser = browser;
        this.categories = categories;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(preferences == null)
            preferences = SimplePreferencesProvider.getInstance(parent.getContext());
        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.recycleritem_note, parent, false);
        return new NoteViewHolder(delegate, itemView);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {

        FirmNote currentNote = browser.getNoteAt(position);
        holder.setID(currentNote.getID());

        if(currentNote.isSelected()) {

            holder.getDateRangeView().setBackgroundColor(
                    holder.itemView.getContext().getColor(R.color.accentDark)
            );

            holder.getNoteHeaderView().setBackgroundColor(
                    holder.itemView.getContext().getColor(R.color.accent)
            );
        }

        else {

            holder.getDateRangeView().setBackgroundColor(
                    holder.itemView.getContext().getColor(
                            SimplePreferencesProvider.getDarkOf(categories.getKeyOf(currentNote.getCategory())))
            );

            holder.getNoteHeaderView().setBackgroundColor(
                    holder.itemView.getContext().getColor(
                            SimplePreferencesProvider.getLightOf(categories.getKeyOf(currentNote.getCategory())))
            );
        }

        holder.getDateRangeView().setText(LooseNote.dateRange(currentNote.getStart(), currentNote.getEnd()));
        holder.getIdView().setText(String.valueOf(currentNote.getID()));
        holder.getTitleView().setText(currentNote.getTitle());
        holder.getContentView().setText(currentNote.getContent());
        holder.getModDatesView().setText(LooseNote.logDates(currentNote.getCreated(), currentNote.getModified()));

        if(DB.SECTION_ARCHIVE.equals(currentNote.getSection())) {

            holder.getTitleView().setPaintFlags(
                    holder.getTitleView().getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG | Paint.STRIKE_THRU_TEXT_FLAG);

            holder.getContentView().setPaintFlags(
                    holder.getContentView().getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        else if(DB.SECTION_TRASHBIN.equals(currentNote.getSection())) {

            holder.getTitleView().setPaintFlags(
                    holder.getTitleView().getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG | Paint.UNDERLINE_TEXT_FLAG);

            holder.getContentView().setPaintFlags(
                    holder.getContentView().getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG | Paint.UNDERLINE_TEXT_FLAG);
        }

        else {

            holder.getTitleView().setPaintFlags(
                    holder.getTitleView().getPaintFlags() & ~(Paint.STRIKE_THRU_TEXT_FLAG | Paint.UNDERLINE_TEXT_FLAG));

            holder.getContentView().setPaintFlags(
                    holder.getTitleView().getPaintFlags() & ~(Paint.STRIKE_THRU_TEXT_FLAG | Paint.UNDERLINE_TEXT_FLAG));
        }
    }

    @Override
    public int getItemCount() {
        return browser.getNotesAmount();
    }
}
