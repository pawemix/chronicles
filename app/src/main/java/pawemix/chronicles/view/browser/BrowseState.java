package pawemix.chronicles.view.browser;

public interface BrowseState {

    void tapNote(int position, Long id);

    void longClickNote(int position, Long id);

    void onBackPressed();

    void onDrawerOpened();

    void onDrawerClosed();

    void toSelectState();

    void toChooseState();

    void toFilterState();

    void onHomeTapped();
}