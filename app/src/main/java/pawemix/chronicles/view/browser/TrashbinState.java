package pawemix.chronicles.view.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.BrowserController;

public class TrashbinState implements SwiperState {

    private final BrowserController browser;

    public TrashbinState(BrowserController browser) {

        this.browser = browser;
    }

    @Override
    public void onSwiped(View rootView, NoteViewHolder holder, int direction) {

        long id = holder.getID();
        if(direction == ItemTouchHelper.RIGHT) {
            browser.restore(id);
            Snackbar.make(rootView, "Successfully restored note.", Snackbar.LENGTH_SHORT).setAction(
                    "Undo", view -> {
                        browser.trash(id);
                        Toast.makeText(rootView.getContext(), "Reverted back to trashbin.", Toast.LENGTH_SHORT).show();
                    }).show();
        }

    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView rv, RecyclerView.ViewHolder vh, float dX, float dY, boolean active) {

        Context context = vh.itemView.getContext();

        if(active) {

            int color = dX < 0? context.getColor(R.color.grayLight) : context.getColor(R.color.greenLight);
            float factor = Math.min(2.0f * Math.abs(dX) / ((float) c.getWidth()), 1.0f);

            Paint paint = new Paint();
            paint.setColor(color);
            paint.setAlpha((int) (factor * 255.0f));

            c.drawRect(rv.getLeft(), vh.itemView.getTop(), rv.getRight(), vh.itemView.getBottom(), paint);
        }
    }
}
