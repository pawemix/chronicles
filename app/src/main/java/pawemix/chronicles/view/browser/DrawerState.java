package pawemix.chronicles.view.browser;

class DrawerState implements BrowseState {

    private final BrowseView view;
    private final BrowseState lastState;

    public DrawerState(BrowseView view, BrowseState lastState) {
        this.view = view;
        this.lastState = lastState;
    }

    @Override
    public void tapNote(int position, Long id) {
        view.closeDrawer();
        view.setState(lastState);
    }

    @Override
    public void longClickNote(int position, Long id) {
        view.closeDrawer();
        view.setState(lastState);
    }

    @Override
    public void onBackPressed() {
        view.closeDrawer();
        view.setState(lastState);
    }

    @Override
    public void onDrawerOpened() {}

    @Override
    public void onDrawerClosed() {
        view.setState(lastState);
    }

    @Override
    public void toSelectState() {
        view.closeDrawer();
        view.setState(lastState);
    }

    @Override
    public void toChooseState() {
        view.closeDrawer();
        view.setState(lastState);
    }

    @Override
    public void toFilterState() {
        view.closeDrawer();
        view.setState(lastState);
    }

    @Override
    public void onHomeTapped() {}
}
