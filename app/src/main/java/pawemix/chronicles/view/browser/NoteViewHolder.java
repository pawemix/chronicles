package pawemix.chronicles.view.browser;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import pawemix.chronicles.R;

public class NoteViewHolder extends RecyclerView.ViewHolder {

    private long id = -1;

    NoteViewHolder(NoteRecyclerDelegate delegate, View itemView) {

        super(itemView);

        TextView idView = itemView.findViewById(R.id.note_id);

        itemView.setOnClickListener(
                view -> delegate.tapNote(getAdapterPosition(), Long.decode(idView.getText().toString())));

        itemView.setOnLongClickListener(view -> {
            delegate.longClickNote(getAdapterPosition(), Long.decode(idView.getText().toString()));
            return true;
        });
    }

    public long getID() {
        return id;
    }

    public void setID(long id) {
        this.id = id;
    }

    public TextView getDateRangeView() {
        return itemView.findViewById(R.id.note_daterange);
    }

    public ConstraintLayout getNoteHeaderView() {
        return itemView.findViewById(R.id.note_header);
    }

    public TextView getIdView() {
        return itemView.findViewById(R.id.note_id);
    }

    public TextView getTitleView() {
        return itemView.findViewById(R.id.note_title);
    }

    public TextView getContentView() {
        return itemView.findViewById(R.id.note_content);
    }

    public TextView getModDatesView() {
        return itemView.findViewById(R.id.note_moddates);
    }
}
