package pawemix.chronicles.view.browser;

import pawemix.chronicles.controller.BrowserController;

public class SelectState implements BrowseState {
    protected final BrowserController controller;
    protected final BrowseView view;

    public SelectState(BrowserController controller, BrowseView view) {
        this.controller = controller;
        this.view = view;
    }

    @Override
    public void tapNote(int position, Long id) {

        controller.swapSelectionForNoteAt(position);

        if(controller.allNotesDeselected()) {
            view.setState(new ChooseState(controller, view));
            view.inflateChooseMenu();
        }
    }

    @Override
    public void longClickNote(int position, Long id) {
        tapNote(position, id);
    }

    @Override
    public void onBackPressed() {
        view.leaveActivity();
    }

    @Override
    public void onDrawerOpened() {
        view.setState(new DrawerState(view, this));
    }

    @Override
    public void onDrawerClosed() {}

    @Override
    public void toSelectState() {}

    @Override
    public void toChooseState() {
        view.setState(new ChooseState(controller, view));
        view.inflateChooseMenu();
    }

    @Override
    public void toFilterState() {
        view.setState(new FilterState(controller, view));
        view.inflateChooseMenu();
    }

    @Override
    public void onHomeTapped() {
        view.openDrawer();
    }
}
