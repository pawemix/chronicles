package pawemix.chronicles.view.browser;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.model.NoteProvider;

public class EmptyTrashbinDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        NoteProvider provider = DatabaseProvider.getInstance(getActivity().getApplicationContext());

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Empty trashbin");
        builder.setMessage("Trashed notes will be removed and lost forever. Are you sure?");
        builder.setPositiveButton("Yes",(dialogInterface, i) -> provider.emptyTrashbin());
        builder.setNegativeButton("No",null);
        return builder.create();
    }
}
