package pawemix.chronicles.view.browser;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;

import java.util.Collections;
import java.util.List;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.BrowserController;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.framework.filter.ColumnOrder;
import pawemix.chronicles.model.DatabaseProvider;

public class SortDialog extends DialogFragment {

    private BrowserController browser;

    private List<String> columns;
    private List<Boolean> ascendings;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        browser = ControllerStack.fetchBrowser(
                BrowseActivity.BROWSER_POSITION,
                this,
                DatabaseProvider.getInstance(getContext())
        );

        columns = browser.getColumnOrder();
        ascendings = browser.getColumnAscendings();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(R.string.visibles_sortbydialog_title);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_sortnotes, null);
        RecyclerView recycler = view.findViewById(R.id.sortnotes_recycler);
        recycler.setAdapter(new SortDialogAdapter(columns, ascendings));
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int oldPos = viewHolder.getAdapterPosition();
                int newPos = target.getAdapterPosition();

                Collections.swap(columns, oldPos, newPos);
                Collections.swap(ascendings, oldPos, newPos);
                recycler.getAdapter().notifyItemMoved(oldPos, newPos);

                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }
        };
        new ItemTouchHelper(callback).attachToRecyclerView(recycler);

        builder.setPositiveButton(
                "Apply", (dialogInterface, i) -> browser.setOrder(new ColumnOrder<>(columns, ascendings)));

        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ControllerStack.abandonBrowser(BrowseActivity.BROWSER_POSITION, this);
    }
}
