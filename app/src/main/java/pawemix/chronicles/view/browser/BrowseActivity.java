package pawemix.chronicles.view.browser;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import pawemix.chronicles.R;
import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.BrowserObserver;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.controller.ObservableBrowserController;
import pawemix.chronicles.framework.filter.ColumnOrder;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.model.PreferencesProvider;
import pawemix.chronicles.model.SimplePreferencesProvider;
import pawemix.chronicles.view.assistant.NoteAssistantService;
import pawemix.chronicles.view.editor.EditActivity;
import pawemix.chronicles.view.settings.CategoryPreference;
import pawemix.chronicles.view.settings.MultiSelectPreference;
import pawemix.chronicles.view.settings.NoteOrderPreference;
import pawemix.chronicles.view.settings.SettingsActivity;

import static android.os.AsyncTask.execute;

public class BrowseActivity extends AppCompatActivity implements BrowseView, NoteRecyclerDelegate, BrowserObserver {

    private static final int IMPORT_REQUEST_CODE = 42;
    public static final int BROWSER_POSITION = 0;

    /**
     * Widok reprezentujący listę notatek.
     */
    private RecyclerView recyclerView;
    Menu menu;
    private DrawerLayout drawerLayout;
    private BrowseState state;
    private SwiperState swipeState;

    private ObservableBrowserController browser;
    private PreferencesProvider preferences;

    /**
     * Ustawia layout, ustawia toolbar, ustawia przycisk w prawym dolnym rogu i ustawia listę.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);

        browser = ControllerStack.fetchBrowser(
                BROWSER_POSITION,
                this,
                DatabaseProvider.getInstance(getApplicationContext())
        );
        browser.addObserver(this);

        preferences = SimplePreferencesProvider.getInstance(getApplicationContext());
        CategoriesController categories = ArrayListCategoriesController.getInstance(
                DatabaseProvider.getInstance(getApplicationContext()));

        if(preferences.assistantOnStartup()) {
            Intent assistant = new Intent();
            assistant.setClass(getBaseContext(), NoteAssistantService.class);
            assistant.setAction(Intent.ACTION_MAIN);
            assistant.addCategory(Intent.CATEGORY_DEFAULT);
            startService(assistant);
        }

        setContentView(R.layout.activity_browse);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setCustomView(R.layout.toolbar_filter);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        recyclerView = findViewById(R.id.browse_recyclerview);
        if(recyclerView != null) {
            recyclerView.setAdapter(new NoteRecyclerAdapter(browser, categories, this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(
                    new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false)
            );

            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(
                    0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    swipeState.onSwiped(recyclerView, (NoteViewHolder) viewHolder, direction);
                }

                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                    swipeState.onChildDraw(c, recyclerView, viewHolder, dX, dY, isCurrentlyActive);
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        swipeState = new NotesState(browser);
        browser.setAutoReload(false);
        browser.setSection(DB.SECTION_DEFAULT);

        browser.setSelectedCategoryNames(MultiSelectPreference.buildTrueStrings(
                prefs.getString(getString(R.string.prefkeys_browserCategories),
                        CategoryPreference.buildDefaultSequence(getApplicationContext())
                )));

        browser.setTextFilter(
                preferences.preferredRegex()); // TODO niech w preferencjach będzie walidacja poprawności regexa

        String orderSequence = prefs.getString(
                getString(R.string.prefkeys_noteOrder), NoteOrderPreference.defaultOrderSequence());

        browser.setOrder(new ColumnOrder<>(
                MultiSelectPreference.buildStrings(orderSequence),
                MultiSelectPreference.buildBooleans(orderSequence)
        ));

        browser.setAutoReload(true);
        browser.reloadNotes();

        FloatingActionButton createNoteButton = findViewById(R.id.browse_createnotebutton);
        if(createNoteButton != null) {

            createNoteButton.setOnClickListener(view -> {

                Intent toEditor = new Intent(Intent.ACTION_EDIT);
                toEditor.setClass(getApplicationContext(), EditActivity.class);
                toEditor.addCategory(Intent.CATEGORY_DEFAULT);
                toEditor.putExtra(getString(R.string.dbkeys_notes_type), false);
                startActivity(toEditor);
            });
        }

        drawerLayout = findViewById(R.id.browse_drawer);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                state.onDrawerOpened();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                state.onDrawerClosed();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
        NavigationView navigationView = findViewById(R.id.browse_drawer_navigation);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(item -> {
            drawerLayout.closeDrawers();
            switch(item.getItemId()) {
                case R.id.menu_browse_notes:
                    browser.setSection(DB.SECTION_DEFAULT);
                    swipeState = new NotesState(browser);
                    break;
                case R.id.menu_browse_archives:
                    browser.setSection(DB.SECTION_ARCHIVE);
                    swipeState = new ArchiveState(browser);
                    break;
                case R.id.menu_browse_trashbin:
                    browser.setSection(DB.SECTION_TRASHBIN);
                    swipeState = new TrashbinState(browser);
                    break;
                case R.id.menu_browse_emptytrashbin:
                    DialogFragment emptyTrashbinDialog = new EmptyTrashbinDialog();
                    emptyTrashbinDialog.show(getFragmentManager(), getString(R.string.tags_emptytrashbindialog));
                    break;
                case R.id.menu_browse_settings:
                    Intent toSettings = new Intent(Intent.ACTION_MAIN);
                    toSettings.setClass(getBaseContext(), SettingsActivity.class);
                    toSettings.addCategory(Intent.CATEGORY_LAUNCHER);
                    startActivity(toSettings);
                    break;
            }
            return true;
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        state = new ChooseState(browser, this);
        inflateChooseMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                state.onHomeTapped();
                return true;
            case R.id.menu_browse_settings:
                Intent toSettings = new Intent(Intent.ACTION_MAIN);
                toSettings.setClass(getBaseContext(), SettingsActivity.class);
                toSettings.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivity(toSettings);
                return true;
            case R.id.menu_browse_filter:
                state.toFilterState();
                return true;
            case R.id.menu_browse_sort:
                DialogFragment sortDialogFragment = new SortDialog();
                sortDialogFragment.show(
                        getFragmentManager(),
                        getBaseContext().getString(R.string.tags_sortdialog)
                );
                return true;
            case R.id.menu_browse_categories:
                DialogFragment filterCategoriesFragment = new FilterCategoriesDialog();
                filterCategoriesFragment.show(
                        getFragmentManager(),
                        getBaseContext().getString(R.string.tags_filtercategoriesdialog)
                );
                return true;
            case R.id.menu_browse_import:
                Intent toImport = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                toImport.addCategory(Intent.CATEGORY_OPENABLE);
                toImport.setType("text/*");
                startActivityForResult(toImport, IMPORT_REQUEST_CODE);
                return true;
            case R.id.menu_browse_export:
                // to be implemented
                return true;
            case R.id.menu_browse_select:
                browser.selectAll();
                state.toSelectState();
                return true;
            case R.id.menu_browse_deselect:
                browser.deselectAll();
                state.toChooseState();
                return true;
            case R.id.menu_browse_restore:
                browser.restoreSelected();
                state.toChooseState();
                Toast.makeText(
                        getBaseContext(), "Selected notes successfully restored.",
                        Toast.LENGTH_SHORT
                ).show();
                return true;
            case R.id.menu_browse_trash:
                browser.trashSelected();
                state.toChooseState();
                Toast.makeText(getBaseContext(), "Selected notes successfully trashed.",
                        Toast.LENGTH_SHORT
                ).show();
                return true;
            case R.id.menu_browse_archive:
                browser.archiveSelected();
                state.toChooseState();
                Toast.makeText(getBaseContext(), "Selected notes successfully archived.",
                        Toast.LENGTH_SHORT
                ).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == IMPORT_REQUEST_CODE && resultCode == RESULT_OK) {
            final Intent resultData = data;
            if(data != null) {
                execute(() -> {
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(
                                resultData.getData());
                        BufferedReader reader = new BufferedReader(
                                new InputStreamReader(inputStream));
                        StringBuilder builder = new StringBuilder();
                        String line;
                        while((line = reader.readLine()) != null) {
                            System.out.println(line);
                            builder.append(line);
                        }
                        inputStream.close();
                        System.out.print(builder.toString());
                    }
                    catch(IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        browser.removeObserver(this);
        browser = null;
        preferences = null;
        ControllerStack.abandonBrowser(BROWSER_POSITION, this);
    }

    @Override
    public void inflateChooseMenu() {

        menu.clear();
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getMenuInflater().inflate(R.menu.menu_browse_choose, menu);
    }

    @Override
    public void inflateSelectMenu() {

        menu.clear();
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getMenuInflater().inflate(R.menu.menu_browse_select, menu);
    }

    @Override
    public void inflateFilterMenu() {

        menu.clear();
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        EditText editText = findViewById(R.id.filter_edittext);
        editText.setText(browser.getTextFilter().pattern());
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    Pattern pattern = Pattern.compile(editable.toString());
                    browser.setTextFilter(pattern);
                }
                catch(PatternSyntaxException ignored) {
                }
            }
        });
    }

    @Override
    public void launchEditor(long id) {

        Intent toEditActivity = new Intent(getBaseContext(), EditActivity.class);
        toEditActivity.setAction(Intent.ACTION_EDIT);
        toEditActivity.addCategory(Intent.CATEGORY_DEFAULT);
        toEditActivity.setData(Uri.parse(getString(R.string.uri_accessnote) + id));
        startActivity(toEditActivity);
    }

    @Override
    public void leaveActivity() {
        finish();
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void setState(BrowseState state) {
        this.state = state;
    }

    @Override
    public void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        state.onBackPressed();
    }

    @Override
    public void tapNote(int position, Long ID) {
        state.tapNote(position, ID);
    }

    @Override
    public void longClickNote(int position, Long ID) {
        state.longClickNote(position, ID);
    }

    // ======== ############### ======== //
    // ======== BrowserObserver ======== //
    // ======== ############### ======== //

    @Override
    public void onNoteUpdatedAt(int position) {
        recyclerView.getAdapter().notifyItemChanged(position);
    }

    @Override
    public void onNoteRemovedAt(int position) {
        recyclerView.getAdapter().notifyItemRemoved(position);
    }

    @Override
    public void onNoteMovedBetween(int from, int to) {
        recyclerView.getAdapter().notifyItemMoved(from, to);
    }

    @Override
    public void onNotesReloaded() {
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onNoteInsertedAt(int position) {
        recyclerView.getAdapter().notifyItemInserted(position);
    }

}
