package pawemix.chronicles.view.browser;

import pawemix.chronicles.controller.BrowserController;

public class ChooseState implements BrowseState {
    private final BrowserController controller;
    private final BrowseView view;

    ChooseState(BrowserController controller, BrowseView view) {
        this.controller = controller;
        this.view = view;
    }

    @Override
    public void tapNote(int position, Long id) {
        view.launchEditor(id);
    }

    @Override
    public void longClickNote(int position, Long id) {
        controller.selectNoteAt(position);
        view.setState(new SelectState(controller, view));
        view.inflateSelectMenu();
    }

    @Override
    public void onBackPressed() {
        view.leaveActivity();
    }

    @Override
    public void onDrawerOpened() {
        view.setState(new DrawerState(view, this));
    }

    @Override
    public void onDrawerClosed() {}

    @Override
    public void toSelectState() {
        view.setState(new SelectState(controller, view));
        view.inflateSelectMenu();
    }

    @Override
    public void toChooseState() {}

    @Override
    public void toFilterState() {
        view.setState(new FilterState(controller, view));
        view.inflateFilterMenu();
    }

    @Override
    public void onHomeTapped() {
        view.openDrawer();
    }
}
