package pawemix.chronicles.view.browser;

/**
 * Interfejs kiedyś służący dla MVC, aby wprowadzać zmiany w aktywności Przeglądarki.
 * Obecnie służy jedynie State'om Przeglądarki, więc ulegnie przemianowaniu.
 */
public interface BrowseView {

    void onBackPressed();

    void launchEditor(long id);

    void leaveActivity();

    void inflateSelectMenu();

    void inflateChooseMenu();

    void setState(BrowseState state);

    void closeDrawer();

    void inflateFilterMenu();

    void openDrawer();
}
