package pawemix.chronicles.view.browser;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pawemix.chronicles.R;

public class SortDialogAdapter extends RecyclerView.Adapter<SortDialogViewHolder> {

    private final List<String> noteColumns;
    private final List<Boolean> ascendings;

    public SortDialogAdapter(List<String> noteColumns, List<Boolean> ascendings) {
        this.noteColumns = noteColumns;
        this.ascendings = ascendings;
    }

    @Override
    public SortDialogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem_notecolumn, parent, false);
        return new SortDialogViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SortDialogViewHolder holder, int position) {
        holder.getTitleText().setText(noteColumns.get(position));
        holder.getCheckBox().setChecked(ascendings.get(position));
        holder.getCheckBox().setOnClickListener(
                view -> ascendings.set(holder.getAdapterPosition(), holder.getCheckBox().isChecked()));
    }

    @Override
    public int getItemCount() {
        return noteColumns.size();
    }
}
