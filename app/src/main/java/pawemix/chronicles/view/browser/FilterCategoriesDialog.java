package pawemix.chronicles.view.browser;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.List;

import pawemix.chronicles.controller.ArrayListCategoriesController;
import pawemix.chronicles.controller.BrowserController;
import pawemix.chronicles.controller.CategoriesController;
import pawemix.chronicles.controller.ControllerStack;
import pawemix.chronicles.model.DatabaseProvider;

public class FilterCategoriesDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        CategoriesController categories = ArrayListCategoriesController.getInstance(
                DatabaseProvider.getInstance(getContext()));
        BrowserController browser = ControllerStack.fetchBrowser(
                BrowseActivity.BROWSER_POSITION,
                this,
                DatabaseProvider.getInstance(getContext())
        );

        List<String> selected = browser.getSelectedCategoryNames();
        List<String> list = categories.getCategoryNames();
        String[] template = new String[list.size()];
        String[] array = list.toArray(template);
        boolean[] selection = new boolean[array.length];
        for(int i = 0; i < array.length; i++) selection[i] = selected.contains(array[i]);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(
                array, selection, (dialogInterface, i, b) -> {
                    selection[i] = b;
                    if(b) selected.add(array[i]);
                    else selected.remove(array[i]);
                    browser.setSelectedCategoryNames(selected);
                });
        return builder.create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ControllerStack.abandonBrowser(BrowseActivity.BROWSER_POSITION, this);
    }
}
