package pawemix.chronicles.view.browser;

public interface NoteRecyclerDelegate {
    void tapNote(int position, Long ID);
    void longClickNote(int position, Long ID);
}
