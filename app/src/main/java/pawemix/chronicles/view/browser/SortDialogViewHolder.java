package pawemix.chronicles.view.browser;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import pawemix.chronicles.R;

public class SortDialogViewHolder extends RecyclerView.ViewHolder{

    private final TextView titleText;
    private final CheckBox checkBox;

    public SortDialogViewHolder(View itemView) {
        super(itemView);
        this.titleText = itemView.findViewById(R.id.notecolumn_title);
        this.checkBox = itemView.findViewById(R.id.notecolumn_check);
    }

    public TextView getTitleText() {
        return titleText;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }
}
