package pawemix.chronicles.model;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import pawemix.chronicles.R;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DatabaseProviderTest {
    private Context appContext;
    private NoteProvider provider;

    @Before
    public void setUp() {

        appContext = InstrumentationRegistry.getTargetContext();
        provider = DatabaseProvider.getInstance(appContext);
    }

    @Test
    public void test_fetchNotes_if_noNotesApplyingToClauses() {

        String whereClause = appContext.getString(R.string.dbkeys_notes_datecreated) + " > ?";
        final String now = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        String[] whereArgs = new String[]{now};

        Collection<Bundle> noteBundles = provider.fetchNotes(whereClause, whereArgs, null);

        assertNotNull(noteBundles);

        assertTrue(noteBundles.isEmpty());
    }

    @Test
    public void test_fetchNote_if_noSuchNoteExists_with_createNote_deleteNote() {

        long id = provider.createNote(new ContentValues());
        provider.deleteNote(id);

        Bundle bundle = provider.fetchNote(id);
        assertNotNull(bundle);
        assertTrue(bundle.isEmpty());

    }

    @Test
    public void test_createNote_with_fetchNote_deleteNote() {

        int beforeCreation = provider.fetchNotes(null, null, null).size();

        long id = provider.createNote(new ContentValues());

        int afterCreation = provider.fetchNotes(null, null, null).size();

        provider.deleteNote(id);

        assertEquals(beforeCreation + 1, afterCreation);
    }

    @Test
    public void test_createNote_fetchNote_with_deleteNote() {

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(appContext.getString(R.string.dbkeys_notes_title), "Dummy test note");

        Long id;

        assertTrue((id = provider.createNote(contentValues)) > 0);

        String whereClause = appContext.getString(R.string.dbkeys_notes_datecreated) + " > ?";
        final String now = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        String[] whereArgs = new String[]{now};

        Collection<Bundle> noteBundles = provider.fetchNotes(whereClause, whereArgs, null);

        assertNotNull(noteBundles);
        assertTrue(noteBundles.isEmpty());
        assertTrue(provider.deleteNote(id));
    }

    @Test
    public void test_trashNote_with_createNote_fetchNotes_deleteNote() {

        long id = provider.createNote(new ContentValues());
        provider.trashNote(id);

        assertEquals(provider.fetchNote(id).getString(appContext.getString(R.string.dbkeys_notes_section)), appContext.getString(R.string.sections_trashbin));

        provider.deleteNote(id);
    }

    @After
    public void tearDown() {

        provider = null;
        appContext = null;
    }

}
