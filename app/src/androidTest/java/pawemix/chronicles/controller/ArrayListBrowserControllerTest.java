package pawemix.chronicles.controller;

import android.content.ContentValues;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import pawemix.chronicles.R;
import pawemix.chronicles.framework.filter.ColumnOrder;
import pawemix.chronicles.framework.note.FirmNote;
import pawemix.chronicles.model.DatabaseProvider;
import pawemix.chronicles.model.DB;
import pawemix.chronicles.model.NoteProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ArrayListBrowserControllerTest {

    private Context appContext;
    private NoteProvider provider;
    private BrowserController controller;

    @Before
    public void setUp() {
        appContext = InstrumentationRegistry.getTargetContext();
        provider = DatabaseProvider.getInstance(appContext);
        controller = new ArrayListBrowserController(provider);

        List<String> columns = new ArrayList<>(1);
        columns.add(DB.NOTES_ID);
        controller.setOrder(new ColumnOrder<>(columns, true));
    }

    @Test
    public void test_onNoteCreated() {
        controller.reloadNotes();

        int start = controller.getNotesAmount();

        provider.createNote(new ContentValues());

        int begin = controller.getNotesAmount();

        assertEquals(start + 1, begin);
    }

    @Test
    public void test_onNoteUpdated_with_onNoteCreated() {
        ContentValues values = new ContentValues();
        values.put(appContext.getString(R.string.dbkeys_notes_title), "Old getTitle");
        long id = provider.createNote(values);

        controller.reloadNotes();

        String oldTitle = controller.getNoteAt(controller.positionOf(id)).getTitle();
        assertTrue(oldTitle.equals("Old getTitle"));

        values.put(appContext.getString(R.string.dbkeys_notes_title), "New getTitle");
        provider.updateNote(id, values);

        String newTitle = controller.getNoteAt(controller.positionOf(id)).getTitle();
        assertEquals(newTitle, "New getTitle");
    }

    @Test
    public void test_selectNote_with_onNoteCreated() {
        controller.reloadNotes();

        long id = provider.createNote(new ContentValues());

        controller.selectNoteAt(controller.positionOf(id));

        assertTrue(controller.getNoteAt(controller.positionOf(id)).isSelected());
    }

    @Test
    public void test_deleteSelected_with_selectNote() {

        controller.reloadNotes();

        int beginning = controller.getNotesAmount();

        long id1 = provider.createNote(new ContentValues());
        long id2 = provider.createNote(new ContentValues());

        controller.selectNoteAt(controller.positionOf(id1));
        controller.selectNoteAt(controller.positionOf(id2));
        controller.deleteSelected();

        int end = controller.getNotesAmount();

        assertEquals(beginning, end);

    }

    @Test
    public void test_archiveSelected_with_selectNote() {
        controller.reloadNotes();
        long id = provider.createNote(new ContentValues());

        controller.selectNoteAt(controller.positionOf(id));
        controller.archiveSelected();

        FirmNote note = controller.getNoteAt(controller.positionOf(id));

        assertTrue(note.getSection().equals(DB.SECTION_ARCHIVE));
    }

    @Test
    public void test_trashNote_with_selectNote() {
        controller.reloadNotes();

        long id = provider.createNote(new ContentValues());
        int begin = controller.getNotesAmount();

        controller.selectNoteAt(controller.positionOf(id));

        controller.trashSelected();
        provider.emptyTrashbin();

        int end = controller.getNotesAmount();

        assertTrue(end < begin);

    }

    @After
    public void tearDown() {
        controller = null;
    }
}
